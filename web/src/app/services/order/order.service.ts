import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Order } from "src/app/models/order.model";
import { OrderLineService } from "../orderLine/order-line.service";
import { Catalog } from "src/app/models/catalog.model";
import { first } from "rxjs/operators";
import { AlertService } from "../alert.service";
import { Socket } from "ngx-socket-io";
import { Observable } from "rxjs/internal/Observable";

@Injectable({
  providedIn: "root",
})
export class OrderService {
  apiURL = "/api";

  back = this.socket.fromEvent<Order>("backk");
  updatedOrders = this.socket.fromEvent<any>("updatedTableOrder");
  deletedOrders = this.socket.fromEvent<any>("deletedOrder");
  createdOrders = this.socket.fromEvent<any>("createdOrder");

  constructor(private http: HttpClient, private socket: Socket) {}

  public getAll() {
    return this.http.get<Order[]>(`${this.apiURL}/order`);
  }

  public getAllTableOrders(id: number) {
    return this.http.get<Order[]>(`${this.apiURL}/order/table/${id}`);
  }

  public getTakeawayOrders() {
    return this.http.get<Order[]>(`${this.apiURL}/order/takeaway`);
  }

  public getPaidOrders() {
    return this.http.get<Order[]>(`${this.apiURL}/order/paid`);
  }

  public addOrder(order: Order) {
    return this.http.post<Order>(`${this.apiURL}/order`, order);
  }

  public getOrder(id: number) {
    return this.http.get<Order>(`${this.apiURL}/order/${id}`);
  }

  public updateOrder(id: number, order: any) {
    return this.http.put<any>(`${this.apiURL}/order/${id}`, order);
  }

  public deleteOrder(id: number) {
    return this.http.delete<Order>(`${this.apiURL}/order/${id}`);
  }

  public realTimeCreatedOrder(order) {
    this.socket.emit("createOrder", {
      idOrder: order.idTable,
      isTakeaway: order.takeaway,
    });
  }

  public realTimeUpdatedOrder(order) {
    this.socket.emit("updateOrder", order);
  }

  public realTimeDeletedOrder(id) {
    this.socket.emit("deleteOrder", { idOrder: id });
  }
}
