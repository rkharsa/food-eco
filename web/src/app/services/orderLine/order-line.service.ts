import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { OrderLine } from "../../models/orderline.model";
import { first } from "rxjs/operators";
import { AlertService } from "../alert.service";
import { Socket } from "ngx-socket-io";

@Injectable({
  providedIn: "root",
})
export class OrderLineService {
  apiURL = "http://localhost:3001";

  updatedOrderLine = this.socket.fromEvent<any>("updatedOrderLine");
  updatedOrder = this.socket.fromEvent<any>("updatedOrder");

  constructor(
    private http: HttpClient,
    private alertService: AlertService,
    private socket: Socket
  ) {}

  getCurrentOrder() {
    return this.http.get<OrderLine[]>(`/api/orderLine/current`);
  }

  getOrderOrderLines(idOrder) {
    return this.http.get<any[]>(`/api/orderLine/${idOrder}`);
  }

  public addOrderLine(orderLine: OrderLine) {
    return this.http.post(`/api/orderLine`, orderLine);
  }

  public RealTimeupdateOrderLine(obj) {
    this.socket.emit("updateOrderLine", obj);
  }

  public RealTimeAddedOrUpdatedOrder(idOrder) {
    return this.socket.emit("updateOrder", { idOrder });
  }

  public realTimeUpdatedTableOrder(id) {
    return this.socket.emit("updateTableOrder", { idOrder: id });
  }

  public updateOrderLine(obj) {
    return this.http
      .put<OrderLine[]>(`/api/orderLine`, obj)
      .pipe(first())
      .subscribe(
        (data) => {
          this.RealTimeupdateOrderLine(obj);
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }

  public deleteOrderOrderLines(id: number) {
    return this.http
      .delete(`/api/orderLine/order/${id}`)
      .pipe(first())
      .subscribe(
        (data) => {},
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  public updateOrderOrderLines(idOrder: number, items: any[]) {
    return this.http
      .delete(`/api/orderLine/order/${idOrder}`)
      .pipe(first())
      .subscribe(
        (data) => {
          const orderLines = [];
          items.map((item) => {
            if (item.OrderLineStatus == 0) {
              orderLines.push(item);
            }
          });
          this.addMultiOrderLines(orderLines, idOrder);
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  public updateStatus(obj) {
    return this.http.put(`/api/orderLine/update/status`, obj);
  }
  public addMultiOrderLines(items: any[], id) {
    items.map((orderLine) => {
      const createdOrderLine: OrderLine = {
        idOrderLine: null,
        quantity: orderLine.quantity,
        price: null,
        comments: orderLine.comments,
        status: null,
        idOrder: id,
        idItem: orderLine.id,
        createdAt: null,
      };

      this.addOrderLine(createdOrderLine)
        .pipe(first())
        .subscribe(
          (data) => {
            this.RealTimeAddedOrUpdatedOrder(id);
            this.realTimeUpdatedTableOrder(id);
          },
          (e) => {
            this.alertService.openSnackBar(e.error.message, "x");
          }
        );
    });
  }
}
