import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AlertService } from "../alert.service";
import { Lot } from "../../models/lot.model";

@Injectable({
  providedIn: "root",
})
export class LotService {
  apiURL = "/api";
  constructor(private http: HttpClient, private alertService: AlertService) {}

  getLotBySupplier(idSupplier: number) {
    return this.http.get<Lot[]>(`${this.apiURL}/lot/supplier/${idSupplier}`);
  }
  addLot(lot: Lot) {
    return this.http.post<Lot>(`${this.apiURL}/lot`, lot);
  }
  updateLot(lot: Lot) {
    return this.http.put(`${this.apiURL}/lot/${lot.idLot}`, lot);
  }
  getLot(idLot: number) {
    return this.http.get<Lot>(`${this.apiURL}/lot/${idLot}`);
  }
  getLots() {
    return this.http.get<Lot[]>(`${this.apiURL}/lot`);
  }
  public getAvailableLots() {
    return this.http.get<any[]>(`${this.apiURL}/lot/isAvailable`);
  }
  public getExpireLots() {
    return this.http.get<any[]>(`${this.apiURL}/lot/expire`);
  }

  public getLotsWithAssets() {
    return this.http.get<any[]>(`${this.apiURL}/lot/asset`);
  }
}
