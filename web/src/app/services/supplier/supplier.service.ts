import { Injectable } from "@angular/core";
import { AlertService } from "../alert.service";
import { HttpClient } from "@angular/common/http";
import { Supplier } from "../../models/supplier.model";

@Injectable({
  providedIn: "root",
})
export class SupplierService {
  constructor(private http: HttpClient, private alertService: AlertService) {}
  deleteSupplier(idSupplier: number) {
    return this.http.delete(`/api/supplier/${idSupplier}`);
  }
  updateSupplier(supplier: Supplier) {
    return this.http.put(`/api/supplier/${supplier.idSupplier}`, supplier);
  }
  addSupplier(supplier: Supplier) {
    return this.http.post(`/api/supplier`, supplier);
  }
  getSuppliers() {
    return this.http.get<any[]>(`/api/supplier`);
  }
  getSupplier(idSupplier: number) {
    return this.http.get<Supplier>(`/api/supplier/${idSupplier}`);
  }
}
