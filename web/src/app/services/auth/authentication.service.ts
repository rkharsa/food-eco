import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { localizedString } from "@angular/compiler/src/output/output_ast";

@Injectable({
  providedIn: "root",
})
export class AuthenticationService {
  apiURL = "/api/";
  constructor(private http: HttpClient) {}

  login(login: string, password: string) {
    return this.http.post<any>(`${this.apiURL}auth/login`, { login, password });
  }

  loggedIn() {
    return !!localStorage.getItem("token");
  }

  logoutUser() {
    localStorage.removeItem("token");
  }

  getToken() {
    return localStorage.getItem("token");
  }
}
