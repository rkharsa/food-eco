import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "src/app/models/user.model";
import { first } from "rxjs/operators";
import { AlertService } from "../alert.service";

@Injectable({
  providedIn: "root",
})
export class UserService {
  apiURL = "/api";
  constructor(private http: HttpClient) {}

  public getAll() {
    return this.http.get<User[]>(`${this.apiURL}/users`);
  }

  register(user: User) {
    return this.http.post(`${this.apiURL}/auth/register`, user);
  }

  public getCurrentUser() {
    return this.http.get(`${this.apiURL}/user`);
  }

  public getAllEmployees() {
    return this.http.get<User[]>(`${this.apiURL}/employees`);
  }

  public getCurrentEmployees() {
    return this.http.get<User[]>(`${this.apiURL}/employees/current`);
  }

  public getOldEmployees() {
    return this.http.get<User[]>(`${this.apiURL}/employees/ended`);
  }

  public updateUser(id: number, user: User) {
    return this.http.put<User[]>(`${this.apiURL}/users/${id}`, user);
  }

  public isAllowed(roles) {
    return this.http.post<any>(`${this.apiURL}/authorized`, roles);
  }
}
