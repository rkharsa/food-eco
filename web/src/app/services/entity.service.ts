import { Injectable } from "@angular/core";
import { Entity } from "../models/entity.model";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class EntityService {
  apiURL = "/api";

  constructor(private http: HttpClient) {}

  public getAll() {
    return this.http.get<Entity[]>(`${this.apiURL}/entity`);
  }

  public getCurrentEntity() {
    return this.http.get<Entity>(`${this.apiURL}/entity/current`);
  }

  addEntity(entity: Entity) {
    return this.http.post<Entity>(`${this.apiURL}/entity`, entity);
  }
}
