import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';

import { UserService } from './services/auth/user.service';
import { AuthenticationService } from './services/auth/authentication.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuard } from './services/auth/auth.guard';
import { TokenInterceptorService } from './services/auth/token-interceptor.service';
import { HomeGuard } from './services/auth/home.guard';
import { AlertService } from './services/alert.service';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AssetService } from './services/asset/asset.service';
import { AssetInProductService } from './services/assetInProduct/asset-in-product.service';
import { CatalogService } from './services/catalog/catalog.service';
import { UnitService } from './services/unit/unit.service';
import { DisableControlDirective } from './directives/disable-control.directive';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const config: SocketIoConfig = { url: '', options: {} };

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: '',
    loadChildren: () => import('./dash-layout/dash-layout.module').then(m => m.DashLayoutModule),
    canActivate: [AuthGuard]
  },
  {
    path: '', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
    canActivate: [HomeGuard]
  }
];


@NgModule({
  declarations: [
    AppComponent,
    DisableControlDirective,
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    RouterModule.forRoot(routes, {
      useHash: true
    }),
    SocketIoModule.forRoot(config),
    HttpClientModule,
    MatSnackBarModule,
    CommonModule,
    NgbModule
  ],
  providers: [UserService,
    AssetService,
    AssetInProductService,
    CatalogService,
    AuthenticationService,
    UnitService,
    AuthGuard,
    HomeGuard,
    AlertService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
