import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "searchfilter",
})
export class SearchfilterPipe implements PipeTransform {
  transform(items: any[], term: string): any[] {
    if (!items || !term) {
      return items;
    }

    return items.filter((item) => {
      const dataStr = JSON.stringify(item).toLowerCase();
      return dataStr.indexOf(term.toLowerCase()) != -1;
    });
  }
}
