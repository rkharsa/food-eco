import { Component, OnInit } from "@angular/core";
import { Table } from "src/app/models/table.model";
import { TableService } from "src/app/services/table/table.service";
import { first, map } from "rxjs/operators";
import { OrderService } from "src/app/services/order/order.service";
import { AlertService } from "src/app/services/alert.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Order } from "src/app/models/order.model";
import { Validators, FormGroup, FormBuilder } from "@angular/forms";
import { OrderLineService } from "src/app/services/orderLine/order-line.service";
import { OrderLine } from "src/app/models/orderline.model";
import { interval, Subscription } from "rxjs";

@Component({
  selector: "app-table",
  templateUrl: "./table.component.html",
  styleUrls: ["./table.component.css"],
})
export class TableComponent implements OnInit {
  tables: Table[] = [];
  takeawayOrders: Order[];
  tableForm: FormGroup;
  table: Table;
  showForm = false;
  orderLineSubscribtion: any;
  showTakeaways = false;
  isFullScreen: boolean;
  private _updatedOrderLine: Subscription;
  private _updatedTables: Subscription;
  private _updatedOrder: Subscription;
  private _createdOrder: Subscription;
  private _deletedOrder: Subscription;
  private _paidOrders: Subscription;

  constructor(
    private tableService: TableService,
    private orderService: OrderService,
    private orderLineService: OrderLineService,
    private alertService: AlertService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.launchSubscriptions();
    this.getAllTables();
    this.getTakeAwayOrders();

    this.tableForm = this.fb.group({
      name: ["", Validators.required],
      seats: ["1", Validators.required],
      description: ["", Validators.required],
    });
  }

  getAllTables() {
    this.tableService
      .getAll()
      .pipe(first())
      .subscribe(
        (data) => {
          this.tables = data;
          if (this.tables != null) {
            this.tables.map((table) => {
              this.getTableOrders(table);
            });
          }
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  getTableOrders(table) {
    this.orderService
      .getAllTableOrders(table.idTable)
      .pipe(first())
      .subscribe(
        (orders) => {
          if (orders != null) {
            orders.map((a) => this.getOrderLines(a));
            table.orders = orders;
          } else {
            table.orders = [];
          }
        },
        (err) => {
          this.alertService.openSnackBar(err.error.message, "x");
        }
      );
  }
  fullScreen() {
    document.getElementById("full").requestFullscreen();
  }
  getOrderLines(order: Order) {
    this.orderLineService
      .getOrderOrderLines(order.idOrder)
      .pipe(first())
      .subscribe(
        (data) => {
          order.orderLines = data;
        },
        (err) => {
          this.alertService.openSnackBar(err.error.message, "x");
        }
      );
  }

  getTakeAwayOrders() {
    this.orderService
      .getTakeawayOrders()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data != null) {
            data.map((a) => this.getOrderLines(a));
            this.takeawayOrders = data;
          } else {
            this.takeawayOrders = [];
          }
        },
        (err) => {
          this.alertService.openSnackBar(err.error.message, "x");
        }
      );
  }

  onSubmitForm() {
    if (this.tableForm.invalid) {
      return;
    }

    this.createTable();
    this.tableService
      .addTable(this.table)
      .pipe(first())
      .subscribe(
        (data) => {
          this.tableService.realTimeUpdatedTable();
          this.getAllTables();
        },
        (err) => {
          this.alertService.openSnackBar(err.error.message, "x");
        }
      );

    this.getAllTables();
    this.tableForm.reset();
    this.showForm = false;
  }

  createTable() {
    this.table = {
      idTable: null,
      name: this.tableForm.value.name,
      numberOfSeats: this.tableForm.value.seats,
      description: this.tableForm.value.description,
      idEntity: null,
      orders: null,
    };
  }

  deleteTable(idTable) {
    this.tableService
      .deleteTable(idTable)
      .pipe(first())
      .subscribe(
        (table) => {
          this.tableService.realTimeUpdatedTable();
          this.getAllTables();
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  onClickUpdateStatus(item: OrderLine) {
    if (item.status == 3) {
      item.status = 2;
    } else if (item.status != 2) {
      return this.alertService.openSnackBar("Commande pas encore cuisiné", "x");
    } else {
      item.status = 3;
    }

    const obj = {
      status: item.status,
      idOrderLine: item.idOrderLine,
      idOrder: item.idOrder,
    };
    this.orderLineService.updateOrderLine(obj);
  }

  payOrder(order) {
    const served = order.orderLines.every((orderLine) => {
      return orderLine.status == 3;
    });

    if (!served) {
      return this.alertService.openSnackBar(
        "La commande n'est pas encore servie",
        "x"
      );
    }
    const obj = { isPaid: true };
    this.orderService
      .updateOrder(order.idOrder, obj)
      .pipe(first())
      .subscribe(
        (data) => {
          this.orderService.realTimeUpdatedOrder({
            idOrder: order.idOrder,
            isPaid: true,
            isTakeaway: order.takeaway,
          });
          this.getAllTables();
          this.getTakeAwayOrders();
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  updateOrders(idOrder) {
    let isTakeaway = true;
    this.tables.map((table) => {
      table.orders.map((order) => {
        if (order.idOrder == idOrder) {
          isTakeaway = false;
          this.getTableOrders(table);
        }
      });
    });
    if (isTakeaway) {
      this.getTakeAwayOrders();
    }
  }

  updateTableOrders(idTable, isTakeaway) {
    if (isTakeaway) {
      this.getTakeAwayOrders();
    } else {
      this.tables.map((table) => {
        if (table.idTable == idTable) {
          isTakeaway = false;
          this.getTableOrders(table);
        }
      });
    }
  }

  updateOrderLine(idOrder) {
    this.tables.map((table) => {
      table.orders.map((order) => {
        if (order.idOrder == idOrder) {
          this.getOrderLines(order);
        }
      });
    });
    this.takeawayOrders.map((order) => {
      if (order.idOrder == idOrder) {
        this.getOrderLines(order);
      }
    });
  }

  closeForm() {
    this.tableForm.reset();
    this.showForm = false;
  }

  launchSubscriptions() {
    this._updatedOrderLine = this.orderLineService.updatedOrderLine.subscribe(
      (data) => {
        this.updateOrderLine(data.idOrder);
      }
    );

    this._updatedTables = this.tableService.updatedTable.subscribe((data) => {
      this.getAllTables();
    });

    this._paidOrders = this.orderLineService.updatedOrder.subscribe((data) => {
      if (data.isPaid) {
        this.updateOrders(data.idOrder);
      } else if (!data.isTakeaway) {
        this.tables.map((table) => {
          if (table.idTable == data.idTable) {
            this.getTableOrders(table);
          }
        });
      } else {
        this.getTakeAwayOrders();
      }
    });

    this._updatedOrder = this.orderService.updatedOrders.subscribe((data) => {
      this.updateOrders(data.idOrder);
    });

    this._createdOrder = this.orderService.createdOrders.subscribe((data) => {
      this.updateTableOrders(data.idOrder, data.isTakeaway);
    });

    this._deletedOrder = this.orderService.deletedOrders.subscribe((data) => {
      this.updateOrders(data.idOrder);
    });
  }

  ngOnDestroy(): void {
    this._updatedOrderLine.unsubscribe();
    this._updatedTables.unsubscribe();
    this._updatedOrder.unsubscribe();
    this._createdOrder.unsubscribe();
    this._deletedOrder.unsubscribe();
    this._paidOrders.unsubscribe();
  }
}
