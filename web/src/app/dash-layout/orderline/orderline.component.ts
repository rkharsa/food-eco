import {
  Component,
  OnInit,
  ViewChild,
  OnChanges,
  OnDestroy,
  Input,
} from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { first } from "rxjs/operators";
import { OrderLineService } from "../../services/orderLine/order-line.service";
// import { pipe, Observable, Subscription } from 'rxjs';
import { OrderLine } from "../../models/orderline.model";
import { AlertService } from "../../services/alert.service";
import { Observable, interval, Subscription } from "rxjs";

@Component({
  selector: "app-orderline",
  templateUrl: "../orderline/orderline.component.html",
  styleUrls: ["./orderline.component.css"],
})

/**
 * @OrderLine Data table with sorting, pagination, and filtering for order.
 */
export class OrderlineComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = [
    "id",
    "title",
    "quantity",
    "comments",
    "createdAt",
    "status",
  ];
  dataSource: MatTableDataSource<any>;
  private _orderLineSubscription: Subscription;
  private _updatedOrders: Subscription;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @Input() perLine = true;
  orderLine: OrderLine[];

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private orderLineService: OrderLineService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.getOrders();

    this._updatedOrders = this.orderLineService.updatedOrder.subscribe(
      (data) => {
        this.getOrders();
      }
    );
    this._orderLineSubscription = this.orderLineService.updatedOrderLine.subscribe(
      (data) => {
        this.getOrders();
      }
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  // TODO mettre a jour que ce qui ne sont pas servie quand on est en modification en groupe
  onClickUpdateStatus(item: OrderLine) {
    let obj;
    if (item.status <= 2) {
      if (!this.perLine) {
        obj = {
          status: item.status,
          idOrder: item.idOrder,
          quantity: item.quantity,
          idItem: item.idItem,
        };
        if (item.status === 2) {
          item.status = 1;
        } else {
          item.status += 1;
        }
        this.dataSource.data.forEach((resp) => {
          if (resp.idOrder === item.idOrder) {
            resp.status = item.status;
          }
        });
        this.dataSource._updateChangeSubscription();
      } else {
        obj = {
          status: item.status,
          idOrderLine: item.idOrderLine,
          idOrder: item.idOrder,
          quantity: item.quantity,
          idItem: item.idItem,
        };
        if (item.status === 2) {
          item.status = 1;
        } else {
          item.status += 1;
        }
      }

      this.orderLineService
        .updateStatus(obj)
        .pipe(first())
        .subscribe(
          (data) => {
            this.orderLineService.RealTimeupdateOrderLine(obj);
          },
          (e) => {}
        );
    }
  }

  getOrders() {
    this.orderLineService
      .getCurrentOrder()
      .pipe(first())
      .subscribe(
        (data) => {
          this.dataSource = new MatTableDataSource<OrderLine>(data);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.dataSource.filterPredicate = (data: any, filter) => {
            const dataStr = JSON.stringify(data).toLowerCase();
            return dataStr.indexOf(filter) != -1;
          };
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }
  ngOnDestroy(): void {
    this._updatedOrders.unsubscribe();
    this._orderLineSubscription.unsubscribe();
  }
}
