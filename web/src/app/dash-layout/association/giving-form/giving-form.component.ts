import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AlertService } from "src/app/services/alert.service";
import { ActivatedRoute, Router } from "@angular/router";
import { LotService } from "src/app/services/lot/lot.service";
import { Lot } from "src/app/models/lot.model";
import { first } from "rxjs/operators";
import { ToGive } from "src/app/models/to-give.model";
import { ToGiveService } from "src/app/services/toGive/to-give.service";

@Component({
  selector: "app-giving-form",
  templateUrl: "./giving-form.component.html",
  styleUrls: ["./giving-form.component.css"],
})
export class GivingFormComponent implements OnInit {
  givingForm: FormGroup;

  lots: any[] = [];
  toGive: ToGive;
  idToGive: number;
  updatedToGive: ToGive;

  currentIdLot = 0;
  maxQuantity = 1;
  currentUnit = "";

  updateMode: boolean;

  constructor(
    private fb: FormBuilder,
    private alertService: AlertService,
    private lotService: LotService,
    private route: ActivatedRoute,
    private toGiveService: ToGiveService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.idToGive = this.route.snapshot.params.idToGive;
    if (this.idToGive) {
      this.getAllLots();
      this.updateMode = true;
      this.getOneToGive();
    } else {
      this.getAvailableLots();
      this.updateMode = false;
    }

    this.givingForm = this.fb.group({
      idLot: [{ value: null, disabled: this.updateMode }, Validators.required],
      description: ["", Validators.required],
      quantity: [0, Validators.required],
    });
  }

  getAvailableLots() {
    this.lotService
      .getAvailableLots()
      .pipe(first())
      .subscribe(
        (data) => {
          this.lots = data;
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  getAllLots() {
    this.lotService
      .getLotsWithAssets()
      .pipe(first())
      .subscribe(
        (data) => {
          this.lots = data;
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  onSubmitForm() {
    this.createToGive();

    if (this.updateMode) {
      this.toGiveService
        .updateToGive(this.idToGive, this.toGive)
        .pipe(first())
        .subscribe(
          (data) => {
            this.router.navigate(["/given"]);
          },
          (e) => {
            this.alertService.openSnackBar(e.error.message, "x");
          }
        );
    } else {
      this.toGiveService
        .addToGive(this.toGive)
        .pipe(first())
        .subscribe(
          (data) => {
            this.router.navigate(["/given"]);
          },
          (e) => {
            this.alertService.openSnackBar(e.error.message, "x");
          }
        );
    }
  }

  onLotChange() {
    if (this.lots != null) {
      this.lots.map((lot) => {
        if (this.currentIdLot == lot.idLot && this.updatedToGive == null) {
          this.maxQuantity = lot.currentQuantity;
          this.currentUnit = lot.Asset.Unit.abrv;
        }
      });
    }
  }

  setQuantity() {
    if (this.lots != null) {
      this.lots.map((lot) => {
        if (lot.idLot == this.updatedToGive.idLot) {
          this.maxQuantity = this.updatedToGive.quantity + lot.currentQuantity;
          this.currentUnit = lot.Asset.Unit.abrv;
        }
      });
    }
  }

  createToGive() {
    this.toGive = {
      idToGive: 0,
      description: this.givingForm.value.description,
      status: 0,
      quantity: this.givingForm.value.quantity,
      idEntity: 0,
      idLot: this.givingForm.value.idLot,
    };
  }

  getOneToGive() {
    this.toGiveService
      .getToGive(this.idToGive)
      .pipe(first())
      .subscribe(
        (data) => {
          this.updatedToGive = data;
          this.setQuantity();
          this.givingForm.setValue({
            idLot: this.updatedToGive.idLot,
            quantity: this.updatedToGive.quantity,
            description: this.updatedToGive.description,
          });
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  deleteToGive() {
    this.toGiveService
      .deleteToGive(this.idToGive)
      .pipe(first())
      .subscribe(
        (data) => {
          this.router.navigate(["/given"]);
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
}
