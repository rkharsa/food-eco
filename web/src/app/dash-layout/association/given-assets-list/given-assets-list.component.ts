import { Component, OnInit, ViewChild } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { MatPaginator } from "@angular/material/paginator";
import { AlertService } from "src/app/services/alert.service";
import { LotService } from "src/app/services/lot/lot.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ToGiveService } from "src/app/services/toGive/to-give.service";
import { first } from "rxjs/operators";

@Component({
  selector: "app-given-assets-list",
  templateUrl: "./given-assets-list.component.html",
  styleUrls: ["./given-assets-list.component.css"],
})
export class GivenAssetsListComponent implements OnInit {
  toGiveRowData: any[] = [];

  displayedColumns: string[] = [
    "idToGive",
    "idLot",
    "isTaken",
    "time",
    "quantity",
    "description",
    "action",
  ];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private alertService: AlertService,
    private lotService: LotService,
    private route: ActivatedRoute,
    private toGiveService: ToGiveService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getGivenBatches();
  }

  getGivenBatches() {
    this.toGiveService
      .getAll()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data != null) {
            this.toGiveRowData = data;
            this.toGiveRowData.map((toGive) => {
              switch (toGive.status) {
                case 0:
                  toGive.status = "Disponible";
                  break;
                case 1:
                  toGive.status = "Résérvé";
                  break;
                case 2:
                  toGive.status = "Délivré";
                  break;
                default:
                  toGive.status = "Disponible";
              }
            });
          } else {
            this.toGiveRowData = [];
          }

          this.dataSource = new MatTableDataSource<any>(this.toGiveRowData);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }

  changeStatus(toGive) {
    const obj = { status: 2, quantity: toGive.quantity, idLot: toGive.idLot };
    this.toGiveService
      .updateStatus(toGive.idToGive, obj)
      .pipe(first())
      .subscribe(
        (data) => {
          this.getGivenBatches();
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
}
