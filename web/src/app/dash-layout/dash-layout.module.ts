import { NgModule } from "@angular/core";
import { AgmCoreModule } from "@agm/core";
import { CommonModule } from "@angular/common";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { Routes, RouterModule } from "@angular/router";
import { DashLayoutComponent } from "./dash-layout.component";
import { TableComponent } from "./table/table.component";
import { MainNavComponent } from "./main-nav/main-nav.component";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { LayoutModule } from "@angular/cdk/layout";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatButtonModule } from "@angular/material/button";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatIconModule } from "@angular/material/icon";
import { MatListModule } from "@angular/material/list";
import { MatMenuModule } from "@angular/material/menu";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { EmployeesComponent } from "./employees/employees.component";
import { CustomTableComponent } from "./custom-table/custom-table.component";
import { AppComponent } from "../app.component";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatSortModule } from "@angular/material/sort";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatTableModule } from "@angular/material/table";
import { MatSliderModule } from "@angular/material/slider";
import { MatTabsModule } from "@angular/material/tabs";
import { MatInputModule } from "@angular/material/input";
import { OrderlineComponent } from "./orderline/orderline.component";
import { AdminCatalogComponent } from "./catalog/admin-catalog/admin-catalog.component";
import { AdminListCatalogComponent } from "./catalog/admin-list-catalog/admin-list-catalog.component";
import {
  NgbPaginationModule,
  NgbAlertModule,
} from "@ng-bootstrap/ng-bootstrap";
import { FilterCatalogPipe } from "./pipe/filter-catalog.pipe";
import { NgxPaginationModule } from "ngx-pagination";
import { AssetFormComponent } from "./assets/asset-form/asset-form.component";
import { TableAssetsComponent } from "./assets/table-assets/table-assets.component";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatNativeDateModule } from "@angular/material/core";
import { MatSelectModule } from "@angular/material/select";
import { AssetComposeProductTabComponent } from "./assets/asset-compose-product-tab/asset-compose-product-tab.component";
import { OrderFormComponent } from "./table/order/order-form/order-form.component";
import { OrderHistoryComponent } from "./table/order/order-history/order-history.component";
import { SearchfilterPipe } from "./pipe/searchfilter.pipe";
import { SupplierListComponent } from "./supplier-list/supplier-list.component";
import { SupplierDetailsComponent } from "./supplier-details/supplier-details.component";
import { SupplierFormComponent } from "./supplier-form/supplier-form.component";
import { SearchByProductSupplierPipe } from "./pipe/search-by-product-supplier.pipe";
import { LotFormComponent } from "./lot-form/lot-form.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ClarityModule } from "@clr/angular";
import { ListLotComponent } from "./list-lot/list-lot.component";
import { GivingFormComponent } from "./association/giving-form/giving-form.component";
import { GivenAssetsListComponent } from "./association/given-assets-list/given-assets-list.component";
import { DonationsComponent } from "./association/donations/donations.component";
import { MatCardModule } from "@angular/material/card";
import { AuthGuard } from "../services/auth/auth.guard";

const routes: Routes = [
  { path: "", redirectTo: "", pathMatch: "full" },
  {
    path: "",
    component: DashLayoutComponent,
    children: [
      { path: "tables", component: TableComponent },
      { path: "employees", component: EmployeesComponent },
      { path: "catalog", component: AdminCatalogComponent },
      { path: "orderline", component: OrderlineComponent },
      { path: "catalog/:idItem", component: AdminCatalogComponent },
      { path: "listCatalog", component: AdminListCatalogComponent },
      { path: "assets", component: TableAssetsComponent },
      { path: "given", component: GivenAssetsListComponent },
      { path: "give", component: GivingFormComponent },
      { path: "give/:idToGive", component: GivingFormComponent },
      { path: "order/history", component: OrderHistoryComponent },
      { path: "order/table/:idTable", component: OrderFormComponent },
      { path: "order/takeaway", component: OrderFormComponent },
      { path: "order/:idOrder", component: OrderFormComponent },
      { path: "donations", component: DonationsComponent },
      { path: "supplier", component: SupplierListComponent },
      { path: "supplier/:idSupplier", component: SupplierDetailsComponent },
      { path: "supplier/edit/:idSupplier", component: SupplierFormComponent },
      { path: "supplier/add/form", component: SupplierFormComponent },
      { path: "lot/add", component: LotFormComponent },
      { path: "lot/edit/:idLot", component: LotFormComponent },
      {
        path: "listLot",
        component: ListLotComponent,
        canActivate: [AuthGuard],
        data: { roles: ["admin"] },
      },
    ],
  },
];

@NgModule({
  declarations: [
    DashLayoutComponent,
    MainNavComponent,
    TableComponent,
    EmployeesComponent,
    CustomTableComponent,
    AdminCatalogComponent,
    AdminListCatalogComponent,
    OrderlineComponent,
    FilterCatalogPipe,
    AssetFormComponent,
    TableAssetsComponent,
    AssetComposeProductTabComponent,
    OrderFormComponent,
    OrderHistoryComponent,
    SearchfilterPipe,
    SupplierListComponent,
    SupplierDetailsComponent,
    SupplierFormComponent,
    SearchByProductSupplierPipe,
    LotFormComponent,
    ListLotComponent,
    GivingFormComponent,
    GivenAssetsListComponent,
    DonationsComponent,
  ],
  imports: [
    ClarityModule,
    NgbModule,
    CommonModule,
    NgxPaginationModule,
    NgbPaginationModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyCtU_gREM0dlzPhS_9a41F2FmXBK09y7Z4",
    }),
    LayoutModule,
    ScrollingModule,
    MatToolbarModule,
    MatSliderModule,
    MatButtonModule,
    MatSidenavModule,
    MatDatepickerModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    FormsModule,
    MatTabsModule,
    MatInputModule,
    MatTableModule,
    MatCheckboxModule,
    MatSortModule,
    MatPaginatorModule,
    MatIconModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
  ],
})
export class DashLayoutModule {}
