import { Component, OnInit, OnChanges } from "@angular/core";
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";
import { AssetService } from "../../../services/asset/asset.service";
import { first } from "rxjs/operators";
import { AssetInProductService } from "../../../services/assetInProduct/asset-in-product.service";
import { Catalog } from "src/app/models/catalog.model";
import { CatalogService } from "../../../services/catalog/catalog.service";
import { AlertService } from "src/app/services/alert.service";
import { ProductInMenuService } from "../../../services/productInMenu/product-in-menu.service";
import { ActivatedRoute, Router, NavigationExtras } from "@angular/router";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";

@Component({
  selector: "app-admin-catalog",
  templateUrl: "./admin-catalog.component.html",
  styleUrls: [
    "./admin-catalog.component.css",
    "../../dash-layout.component.css",
  ],
})
export class AdminCatalogComponent implements OnInit {
  catalogForm: FormGroup;
  assetTab: object[];
  isMenu: boolean;
  isDuplicateItem: boolean;
  updateMode: boolean;
  catalogGet: Catalog;
  idItem: number;
  constructor(
    private fb: FormBuilder,
    private assetService: AssetService,
    private assetInProductService: AssetInProductService,
    private catalogService: CatalogService,
    private alertService: AlertService,
    private productInMenuService: ProductInMenuService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.isMenu = false;
    this.idItem = this.route.snapshot.params.idItem;
    if (this.idItem) {
      this.updateMode = true;
      this.getOneItem(this.idItem);
    } else {
      this.updateMode = false;
      this.loadOptionType();
    }
    this.initForm();
  }
  loadOptionType() {
    if (this.isMenu) {
      this.getProducts();
    } else {
      this.getAssets();
    }
  }

  initForm() {
    this.catalogForm = this.fb.group({
      title: ["", Validators.required],
      price: [1, Validators.required],
      promote: [false, Validators.required],
      online: [false, Validators.required],
      assets: this.fb.array([
        this.fb.group({
          id: ["", Validators.required],
          quantity: ["1", Validators.required],
        }),
      ]),
    });
  }

  duplicateEntry() {
    const formValue = this.catalogForm.value;

    if (
      new Set(formValue.assets.map((item) => parseInt(item.id, 16))).size !==
      formValue.assets.length
    ) {
      this.isDuplicateItem = true;
    } else {
      this.isDuplicateItem = false;
    }
  }
  getProducts() {
    this.catalogService
      .getProduct()
      .pipe(first())
      .subscribe(
        (data) => {
          this.assetTab = data;
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  getOneItem(idItem) {
    this.catalogService
      .getItem(idItem)
      .pipe(first())
      .subscribe(
        (catalog) => {
          this.catalogGet = catalog;
          this.isMenu = this.catalogGet.isMenu;
          this.loadOptionType();
          this.patchDataInForm(catalog);
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  patchDataInForm(catalog: Catalog) {
    const formValue = this.catalogForm;
    formValue.patchValue({
      title: catalog.title,
      price: catalog.price,
      online: catalog.online,
      promote: catalog.promote,
    });
    this.deleteAsset(0);
    if (catalog.isMenu) {
      this.getProductInMenu();
    } else {
      this.getAssetsInProduct();
    }
  }
  getAssetsInProduct() {
    this.assetInProductService
      .getAssetsInProduct(this.catalogGet.idItem)
      .pipe(first())
      .subscribe(
        (data) => {
          data.map((asset) => {
            this.addAsset(asset.idAsset, asset.quantity);
          });
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  getCatalogIdOrAssetId(obj) {
    if (obj.hasOwnProperty("title")) {
      return obj.idItem;
    } else {
      return obj.idAsset;
    }
  }
  getProductInMenu() {
    this.productInMenuService
      .getProductInMenu(this.catalogGet.idItem)
      .pipe(first())
      .subscribe(
        (data) => {
          data.map((product) =>
            this.addAsset(product.idProduct, product.quantity)
          );
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  OnChangeType() {
    if (this.isMenu) {
      this.isMenu = false;
    } else {
      this.isMenu = true;
    }
    this.loadOptionType();
  }
  getCatalogTitleOrAssetLabel(obj) {
    if (obj.hasOwnProperty("title")) {
      return obj.title;
    } else {
      return obj.label;
    }
  }
  getAssets() {
    this.assetService
      .getAssets()
      .pipe(first())
      .subscribe(
        (response) => {
          this.assetTab = response;
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  // TODO add ingredient form  for add new ingredient if  he have to

  get labelAsset() {
    return this.catalogForm.get("assets") as FormArray;
  }
  addAsset(idAssetOrProduct = null, quantity = null) {
    this.labelAsset.push(
      this.fb.group({
        id: [
          idAssetOrProduct == null ? "" : idAssetOrProduct,
          Validators.required,
        ],
        quantity: [quantity == null ? "1" : quantity, Validators.required],
      })
    );
  }

  deleteAsset(index) {
    this.labelAsset.removeAt(index);
    this.duplicateEntry();
  }
  onSubmitForm() {
    const formValue = this.catalogForm.value;
    const catalog = new Catalog(
      this.idItem ? this.idItem : 0,
      formValue.title,
      formValue.price,
      this.isMenu,
      formValue.online,
      formValue.promote
    );
    let navigationExtras: NavigationExtras;
    if (this.updateMode) {
      navigationExtras = { state: { data: "Mise  à jour reussie " } };
      this.updateCatalog(formValue.assets, catalog);
    } else {
      this.addCatalog(formValue.assets, catalog);
    }
  }
  updateCatalog(tabAssetOrProduct, catalog: Catalog) {
    this.catalogService
      .updateCatalog(catalog)
      .pipe(first())
      .subscribe(
        (data) => {
          if (catalog.isMenu) {
            this.productInMenuService
              .deleteProductsInMenu(catalog.idItem)
              .pipe(first())
              .subscribe(
                (data) => {
                  this.productInMenuService.addMultiProductInMenu(
                    tabAssetOrProduct,
                    catalog.idItem
                  );
                },
                (e) => {
                  this.alertService.openSnackBar(e.message, "x");
                }
              );
          } else {
            this.assetInProductService
              .deleteAssetsInProduct(catalog.idItem)
              .pipe(first())
              .subscribe(
                (data) => {
                  this.assetInProductService.addMultiAssetInProduct(
                    tabAssetOrProduct,
                    catalog.idItem
                  );
                },
                (e) => {
                  this.alertService.openSnackBar(e.message, "x");
                }
              );
          }
          const navigationExtras = { state: { data: "Mise  à jour reussie " } };
          this.router.navigate(["/listCatalog"], navigationExtras);
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  addCatalog(tabAssetOrProduct, catalog: Catalog) {
    const navigationExtras = { state: { data: "Création réussie " } };
    this.catalogService
      .addCatalog(catalog)
      .pipe(first())
      .subscribe(
        (data) => {
          this.catalogService.addProductOrAsset(tabAssetOrProduct, data);
          this.router.navigate(["/listCatalog"], navigationExtras);
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
}
