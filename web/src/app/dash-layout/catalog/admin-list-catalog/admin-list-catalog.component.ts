import { Component, OnInit, ElementRef } from "@angular/core";
import { Catalog } from "../../../models/catalog.model";
import { CatalogService } from "../../../services/catalog/catalog.service";
import { pipe } from "rxjs";
import { first, findIndex } from "rxjs/operators";
import { AlertService } from "../../../services/alert.service";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BreakpointObserver } from "@angular/cdk/layout";
import { Router } from "@angular/router";

@Component({
  selector: "app-admin-list-catalog",
  templateUrl: "./admin-list-catalog.component.html",
  styleUrls: ["./admin-list-catalog.component.css"],
})
export class AdminListCatalogComponent implements OnInit {
  itemsLength = 5;
  items: Catalog[];
  formGroupSearch: FormGroup;
  btnFilterActive = 2;
  p = 1;
  msg = "";
  constructor(
    private catalogService: CatalogService,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as { data: string };
    if (state) {
      this.msg = state.data;
      this.alertService.openSnackBar(this.msg, "x");
    }
  }

  ngOnInit(): void {
    this.getItems();
    this.initFormSearch();
  }

  initFormSearch() {
    this.formGroupSearch = this.formBuilder.group({
      search: "",
    });
  }

  getItems() {
    this.catalogService
      .getCatalogs()
      .pipe(first())
      .subscribe(
        async (response) => {
          if (response != null) {
            this.items = response;
            this.itemsLength = this.items.length;
            this.p = 1;
            this.btnFilterActive = 2;
          }
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  loadProduct() {
    this.catalogService
      .getProduct()
      .pipe(first())
      .subscribe(
        (products) => {
          this.items = products;
          this.p = 1;
          this.btnFilterActive = 0;
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  loadMenu() {
    this.catalogService
      .getMenu()
      .pipe(first())
      .subscribe(
        (menus) => {
          this.items = menus;
          this.p = 1;
          this.btnFilterActive = 1;
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  updateOnline(item: Catalog) {
    this.items[this.items.indexOf(item)].online = !item.online;
    const obj = new Object({ idItem: item.idItem, online: item.online });
    this.catalogService
      .updateCatalog(obj)
      .pipe(first())
      .subscribe(
        (data) => {},
        (e) => {
          this.alertService.openSnackBar(e.error.message, "x");
        }
      );
  }
  deleteItem(item: Catalog) {
    this.items.splice(this.items.indexOf(item), 1);
    this.catalogService.deleteCatalog(item.idItem);
  }
}
