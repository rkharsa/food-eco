import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { SupplierService } from 'src/app/services/supplier/supplier.service';
import { AlertService } from 'src/app/services/alert.service';
import { Supplier } from 'src/app/models/supplier.model';
import { first } from 'rxjs/operators';
import { SupplierDetailsService } from '../../services/supplierDetails/supplier-details.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SupplierDetails } from '../../models/supplierDetails.model';
import { AssetService } from '../../services/asset/asset.service';

@Component({
  selector: 'app-supplier-form',
  templateUrl: './supplier-form.component.html',
  styleUrls: ['./supplier-form.component.css'],
})
export class SupplierFormComponent implements OnInit {
  isDuplicateItem: boolean;
  supplierFormGroup: FormGroup;
  idSupplier: number;
  assetTab: object[];
  updateMode: boolean;
  constructor(
    private fb: FormBuilder,
    private supplierService: SupplierService,
    private alertService: AlertService,
    private supplierDetailsService: SupplierDetailsService,
    private route: ActivatedRoute,
    private router: Router,
    private assetService: AssetService
  ) {}
  ngOnInit() {
    this.idSupplier = this.route.snapshot.params.idSupplier;
    this.supplierFormGroup = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      tel: ['', Validators.required],
      address: ['', Validators.required],
      supplierDetails: this.fb.array([
        this.fb.group({ id: ['', Validators.required] }),
      ]),
    });
    if (this.idSupplier) {
      this.updateMode = true;
      this.getSupplier();
      this.getSupplierDetails();
    } else {
      this.updateMode = false;
    }
    this.getAssets();
  }
  duplicateEntry() {
    const formValue = this.supplierFormGroup.value;

    if (
      new Set(formValue.supplierDetails.map((item) => parseInt(item.id, 16)))
        .size !== formValue.supplierDetails.length
    ) {
      this.isDuplicateItem = true;
    } else {
      this.isDuplicateItem = false;
    }
  }

  get supplierDetails() {
    return this.supplierFormGroup.get('supplierDetails') as FormArray;
  }
  addSupplierDetails(id = null) {
    this.supplierDetails.push(
      this.fb.group({
        id: [id == null ? '' : id, Validators.required],
      })
    );
    this.duplicateEntry();
  }
  deleteSupplierDetails(index) {
    this.supplierDetails.removeAt(index);
    this.duplicateEntry();
  }
  getSupplier() {
    this.supplierService
      .getSupplier(this.idSupplier)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data == null) {
            this.router.navigate(['/supplier']);
          }
          this.supplierFormGroup.patchValue({
            name: data.name,
            email: data.email,
            tel: data.tel,
            address: data.address,
          });
        },
        (e) => {
          this.alertService.openSnackBar(e.message, 'x');
        }
      );
  }
  getAssets() {
    this.assetService
      .getAssets()
      .pipe(first())
      .subscribe(
        (response) => {
          this.assetTab = response;
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, 'x');
        }
      );
  }
  getSupplierDetails() {
    this.supplierDetailsService
      .getSuppliersDetails(this.idSupplier)
      .pipe(first())
      .subscribe(
        (data) => {
          this.deleteSupplierDetails(0);
          data.map((item) => {
            this.addSupplierDetails(item.idAsset);
          });
        },
        (e) => {
          this.alertService.openSnackBar(e.message, 'x');
        }
      );
  }
  updateSupplier(supplier: Supplier) {
    this.supplierService
      .updateSupplier(supplier)
      .pipe(first())
      .subscribe(
        (data) => {},
        (e) => {
          this.alertService.openSnackBar(e.message, 'x');
        }
      );
  }
  deleteSupplierCatalog() {
    this.supplierDetailsService
      .deleteSupplierDetails(this.idSupplier)
      .pipe(first())
      .subscribe(
        (data) => {},
        (e) => {
          this.alertService.openSnackBar(e.message, 'x');
        }
      );
  }
  onSubmit() {
    const formValue = this.supplierFormGroup.value;
    const supplier = new Supplier(
      this.idSupplier == null ? 0 : this.idSupplier,
      formValue.address,
      formValue.email,
      formValue.tel,
      0,
      0,
      formValue.name
    );

    if (this.updateMode) {
      this.updateSupplier(supplier);
      this.deleteSupplierCatalog();
      this.supplierDetailsService.addMultiSupplierDetails(
        formValue.supplierDetails,
        this.idSupplier
      );
    } else {
      this.addSupplierWithMultiSupplierDetails(formValue, supplier);
    }
  }
  addSupplierWithMultiSupplierDetails(formValue, supplier: Supplier) {
    this.supplierService
      .addSupplier(supplier)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.supplierDetailsService.addMultiSupplierDetails(
            formValue.supplierDetails,
            data.idSupplier
          );
          this.router.navigate(['/supplier']);
        },
        (e) => {
          this.alertService.openSnackBar(e.error.message, 'x');
        }
      );
  }
}
