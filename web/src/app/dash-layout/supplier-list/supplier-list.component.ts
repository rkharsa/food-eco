import { Component, OnInit } from "@angular/core";
import { Supplier } from "src/app/models/supplier.model";
import { AlertService } from "src/app/services/alert.service";
import { SupplierService } from "src/app/services/supplier/supplier.service";
import { first } from "rxjs/operators";
import { FormGroup, FormBuilder } from "@angular/forms";
import { SupplierDetails } from "../../models/supplierDetails.model";
import { SupplierDetailsService } from "../../services/supplierDetails/supplier-details.service";
import { EntityService } from "src/app/services/entity.service";
import { GoogleMapsService } from "src/app/services/google-maps/google-maps.service";

@Component({
  selector: "app-supplier-list",
  templateUrl: "./supplier-list.component.html",
  styleUrls: ["./supplier-list.component.css"],
})
export class SupplierListComponent implements OnInit {
  lat: number;
  lon: number;
  radius = 1000;
  radiusDisplay = "1 Km";

  currentEntity: string;
  radiusSearch = false;

  filteredSuppliers: any[];

  suppliers: any[];
  idInputLabelUpdate: number;
  products: SupplierDetails[];
  formGroupSearch: FormGroup;
  p = 1;
  supplierLength = 5;
  constructor(
    private supplierService: SupplierService,
    private alertService: AlertService,
    private fb: FormBuilder,
    private entityService: EntityService,
    private googleMap: GoogleMapsService,
    private supplierDetailsService: SupplierDetailsService
  ) {}

  ngOnInit(): void {
    this.getSuppliers();
    this.initFormSearch();
    this.getProductsSupplier();
    this.setMapFirstLocation();
  }
  initFormSearch() {
    this.formGroupSearch = this.fb.group({
      search: "",
    });
  }
  deleteSupplier(supplier: Supplier) {
    this.suppliers.splice(this.suppliers.indexOf(supplier), 1);
    this.supplierService
      .deleteSupplier(supplier.idSupplier)
      .pipe(first())
      .subscribe(
        (data) => {
          if (this.radiusSearch) {
            this.showHideMarkers();
            this.filterSuppliersByMarker();
          }
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }
  getSuppliers() {
    this.supplierService
      .getSuppliers()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data != null) {
            data.map((supplier) => {
              supplier.isShown = true;
            });
            this.suppliers = data;
            this.filteredSuppliers = data;
            this.supplierLength = data.length;
            this.p = 1;
          }
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }
  getProductsSupplier() {
    this.supplierDetailsService
      .getAllSuppliersDetails()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data != null) {
            this.products = data;
          }
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }

  setMapFirstLocation() {
    this.entityService
      .getCurrentEntity()
      .pipe(first())
      .subscribe(
        (data) => {
          this.currentEntity = data.name;
          this.lat = data.latitude;
          this.lon = data.longitude;
        },
        (e) => {
          this.alertService.openSnackBar(e.message, "x");
        }
      );
  }

  event(type, $event) {
    this.radius = $event;
    this.radiusDisplay = ($event / 1000).toFixed(2) + " Km";
    this.showHideMarkers();
    this.filterSuppliersByMarker();
  }

  filterSuppliersByMarker() {
    const list = [];
    this.suppliers.map((supplier) => {
      if (supplier.isShown) {
        list.push(supplier);
      }
    });
    this.filteredSuppliers = list;
  }

  showHideMarkers() {
    this.suppliers.forEach((value) => {
      value.isShown = this.googleMap.getDistanceBetween(
        value.latitude,
        value.longitude,
        this.lat,
        this.lon,
        this.radius
      );
    });
  }

  changeRadiusSearch(on) {
    this.radiusSearch = on;
    this.suppliers.map((entity) => {
      if (on) {
        entity.isShown = this.googleMap.getDistanceBetween(
          entity.latitude,
          entity.longitude,
          this.lat,
          this.lon,
          this.radius
        );
        this.filterSuppliersByMarker();
      } else {
        entity.isShown = true;
        this.unfilterSuppliers();
      }
    });
  }

  unfilterSuppliers() {
    this.filteredSuppliers = this.suppliers;
    if (this.radiusSearch) {
      this.filterSuppliersByMarker();
      this.showHideMarkers();
    }
  }
}
