import { Component, OnInit } from "@angular/core";
import {
  TablePaginationSettingsModel,
  ColumnSettingsModel,
} from "src/app/dash-layout/custom-table/table-settings.model";
import { User } from "src/app/models/user.model";
import { UserService } from "src/app/services/auth/user.service";
import { AlertService } from "src/app/services/alert.service";
import { first } from "rxjs/operators";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: "app-employees",
  templateUrl: "./employees.component.html",
  styleUrls: ["./employees.component.css"],
})
export class EmployeesComponent implements OnInit {
  showCurrent: boolean;
  formGroup: FormGroup;
  minDate: Date;
  birthMaxDate: Date;
  minEndDate: Date;

  showForm = false;
  updateForm = false;

  user: User;
  currentUser: any;
  rowId: number;

  columnDefinition: ColumnSettingsModel[] = [];
  tablePaginationSettings: TablePaginationSettingsModel = {} as TablePaginationSettingsModel;

  currentUsersRowData: User[] = [];
  oldUsersRowData: User[] = [];
  allRowData: User[] = [];

  constructor(
    private userService: UserService,
    private alert: AlertService,
    private fb: FormBuilder
  ) {
    this.tablePaginationSettings.enablePagination = true;
    this.tablePaginationSettings.pageSize = 5;
    this.tablePaginationSettings.pageSizeOptions = [5, 10, 15];
    this.tablePaginationSettings.showFirstLastButtons = true;
    this.columnDefinition = [
      { name: "id", displayName: "id", disableSorting: false },
      { name: "firstName", displayName: "Prénom", disableSorting: false },
      { name: "lastName", displayName: "Nom", disableSorting: false },
      { name: "email", displayName: "E-mail", disableSorting: false },
      {
        name: "contractStart",
        displayName: "Début Contrat",
        disableSorting: false,
      },
      {
        name: "contractEnd",
        displayName: "Fin Contrat",
        disableSorting: false,
      },
      { name: "idRole", displayName: "Role", disableSorting: false },
    ];
    const currentYear = new Date().getFullYear();
    const currentMonth = new Date().getMonth();
    const currentDay = new Date().getDay();
    this.minDate = new Date(currentYear, currentMonth, currentDay);
    this.minEndDate = new Date(currentYear, currentMonth + 1, currentDay);
    this.birthMaxDate = new Date(currentYear - 18, currentMonth, currentDay);
  }

  ngOnInit(): void {
    this.getUsers();
    this.userService
      .getCurrentUser()
      .pipe(first())
      .subscribe(
        (data) => {
          this.currentUser = data;
        },
        (e) => {
          this.alert.openSnackBar(e.error.message, "x");
        }
      );
    this.formGroup = this.fb.group({
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      dateOfBirth: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]],
      login: ["", Validators.required],
      role: ["", Validators.required],
      password: ["", [Validators.required, Validators.minLength(6)]],
      contractStart: ["", Validators.required],
      contractEnd: ["", Validators.required],
    });
  }

  get f() {
    return this.formGroup.controls;
  }

  getUsers() {
    this.userService
      .getOldEmployees()
      .pipe(first())
      .subscribe(
        (data) => {
          this.oldUsersRowData = data;
        },
        (e) => {
          this.alert.openSnackBar(e.error.message, "x");
        }
      );
    this.userService
      .getCurrentEmployees()
      .pipe(first())
      .subscribe(
        (data) => {
          this.currentUsersRowData = data;
        },
        (e) => {
          this.alert.openSnackBar(e.error.message, "x");
        }
      );
    this.userService
      .getAllEmployees()
      .pipe(first())
      .subscribe(
        (data) => {
          this.allRowData = data;
        },
        (e) => {
          this.alert.openSnackBar(e.error.message, "x");
        }
      );
  }

  onSubmit() {
    // stop here if form is invalid
    if (
      new Date(this.formGroup.controls.contractStart.value) >
      new Date(this.formGroup.controls.contractEnd.value)
    ) {
      this.alert.openSnackBar("Date de fin de contrat non valide", "x");
      return;
    }
    if (this.formGroup.invalid) {
      return;
    }
    if (!this.updateForm) {
      this.createUser();
      this.registerUser();
    } else {
      let a = 0;
      (Object as any).values(this.formGroup.controls).forEach((c) => {
        if (!c.touched) {
          a++;
        }
      });
      if (Object.keys(this.formGroup.controls).length == a) {
        return this.alert.openSnackBar("Aucune modification détécté", "x");
      }
      this.createUser();
      this.updateUser();
    }
  }

  registerUser() {
    this.userService
      .register(this.user)
      .pipe(first())
      .subscribe(
        (data) => {
          this.alert.openSnackBar("Inscription Réussie", "x");
          this.formGroup.reset();
          this.showForm = false;
          this.getUsers();
        },
        (e) => {
          this.alert.openSnackBar(e.error.message, "x");
        }
      );
  }

  updateUser() {
    this.userService
      .updateUser(this.rowId, this.user)
      .pipe(first())
      .subscribe(
        (data) => {
          this.alert.openSnackBar("Modification réussie", "x");
          this.formGroup.reset();
          this.showForm = false;
          this.updateForm = false;
          this.getUsers();
        },
        (e) => {
          this.alert.openSnackBar(e.error.message, "x");
        }
      );
  }

  createUser() {
    this.user = {
      id: null,
      firstName: this.formGroup.value.firstName,
      lastName: this.formGroup.value.lastName,
      dateOfBirth: this.formGroup.value.dateOfBirth,
      email: this.formGroup.value.email,
      login: this.formGroup.value.login,
      password: this.formGroup.value.password,
      role: this.formGroup.value.role,
      idEntity: this.currentUser.idEntity,
      idRole: null,
      contractEnd: this.formGroup.value.contractEnd,
      contractStart: this.formGroup.value.contractStart,
    };
  }

  openForm() {
    this.formGroup.reset();
    this.showForm = true;
    this.updateForm = false;
  }

  disableFormControls(isSelected: boolean) {
    this.showForm = true;
    this.updateForm = true;
  }

  setCurrentId(id: number) {
    this.rowId = id;
  }
}
