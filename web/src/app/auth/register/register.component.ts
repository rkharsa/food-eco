import { Component, OnInit } from "@angular/core";
import { User } from "src/app/models/user.model";
import { UserService } from "src/app/services/auth/user.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { first } from "rxjs/operators";
import { Entity } from "src/app/models/entity.model";
import { EntityService } from "src/app/services/entity.service";
import { Router } from "@angular/router";
import { AlertService } from "src/app/services/alert.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"],
})
export class RegisterComponent implements OnInit {
  user: User;
  entity: Entity;
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  showForm = false;
  isAssociationForm: boolean;
  disableRestaurantButton: boolean;
  disableAssociationButton: boolean;

  constructor(
    private userService: UserService,
    private entityService: EntityService,
    private fb: FormBuilder,
    private router: Router,
    private alert: AlertService
  ) {}

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      dateOfBirth: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]],
      login: ["", Validators.required],
      password: ["", [Validators.required, Validators.minLength(6)]],
      name: ["", Validators.required],
      adress: ["", Validators.required],
      city: ["", Validators.required],
      tel: ["", Validators.required],
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.createEntity();
    this.addEntity();
  }

  addEntity() {
    this.entityService
      .addEntity(this.entity)
      .pipe(first())
      .subscribe(
        (data) => {
          this.createUser(data.idEntity);
          this.registerUser();
          this.loading = false;
          this.alert.openSnackBar("Inscription Réussi", "x");
          this.router.navigate(["/login"]);
        },
        (e) => {
          this.loading = false;
          this.alert.openSnackBar(e.error.message, "x");
        }
      );
  }

  registerUser() {
    this.userService
      .register(this.user)
      .pipe(first())
      .subscribe(
        (data) => {},
        (e) => {
          this.alert.openSnackBar(e.error.message, "x");
        }
      );

    this.loading = true;
  }

  createUser(idCreatedEntity) {
    this.user = {
      id: null,
      firstName: this.registerForm.value.firstName,
      lastName: this.registerForm.value.lastName,
      dateOfBirth: this.registerForm.value.dateOfBirth,
      email: this.registerForm.value.email,
      login: this.registerForm.value.login,
      password: this.registerForm.value.password,
      role: this.isAssociationForm ? "association" : "admin",
      idEntity: idCreatedEntity,
      idRole: null,
      contractEnd: null,
      contractStart: null,
    };
  }

  createEntity() {
    this.entity = {
      idEntity: null,
      name: this.registerForm.value.name,
      isAssociation: this.isAssociationForm,
      address: this.registerForm.value.adress,
      city: this.registerForm.value.city,
      tel: this.registerForm.value.tel,
      longitude: null,
      latitude: null,
    };
  }

  activateAssociationForm() {
    this.showForm = true;
    this.disableAssociationButton = true;
    this.disableRestaurantButton = false;
    this.isAssociationForm = true;
  }

  activateRestaurantForm() {
    this.showForm = true;
    this.disableAssociationButton = false;
    this.disableRestaurantButton = true;
    this.isAssociationForm = false;
  }
}
