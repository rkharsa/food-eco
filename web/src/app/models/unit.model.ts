export class Unit {
  constructor(
    public idUnit: number,
    public label: string,
    public abrv: string
  ) {}
}
