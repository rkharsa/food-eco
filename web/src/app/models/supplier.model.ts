export class Supplier {
    constructor(
        public idSupplier: number,
        public address: string,
        public email: string,
        public tel: number,
        public latitude: number,
        public longitude: number,
        public name: string
    ) { }
}
