export class Asset {
  constructor(
    public idAsset: number,
    public idEntity: number,
    public label: string,
    public idUnit: number
  ) {}
}
