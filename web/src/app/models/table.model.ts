import { Order } from './order.model';

export class Table {
    idTable: number;
    description: string;
    name: string;
    numberOfSeats: number;
    idEntity: number;
    orders: Order[];
}
