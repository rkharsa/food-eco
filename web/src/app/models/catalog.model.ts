export class Catalog {
    constructor(
        public idItem: number,
        public title: string,
        public price: number,
        public isMenu: boolean,
        public online: boolean,
        public promote: boolean
    ) { }
}
