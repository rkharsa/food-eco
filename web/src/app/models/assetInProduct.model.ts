export class AssetInProduct {
    constructor(
        public idAsset: number,
        public label: string,
        public idProduct: number,
        public title: string,
        public quantity: number,
    ) { }
}
