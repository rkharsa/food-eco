export class Entity {
    idEntity: number;
    name: string;
    isAssociation: boolean;
    address: string;
    city: string;
    tel: number;
    longitude: number;
    latitude: number;
}
