export class OrderLine {
    constructor(
        public idOrderLine: number,
        public quantity: number,
        public price: number,
        public comments: string,
        public status: number,
        public idOrder: number,
        public idItem: number,
        public createdAt: Date
    ) { }
}
