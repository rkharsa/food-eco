const bodyParser = require('body-parser');
const EntityController = require('../controllers').EntityController;
const AuthMiddleware = require('../middlewares').AuthMiddleware;

module.exports = function (app) {
    app.post('/api/entity', bodyParser.json(), async (req, res) => {
        if (req.body.address && req.body.name && req.body.city && req.body.tel && (req.body.isAssociation == true || req.body.isAssociation == false)) {
            return await EntityController.addEntity(req,res);
        } else {
            return res.status(400).end();
        }
    });

    app.put('/api/entity/:idEntity', bodyParser.json(), async (req, res) => {
        if (req.params.idEntity && (req.body.isAssociation || req.body.name || req.body.address || req.body.city || req.body.tel)) {
            return await EntityController.updateEntity(req, res);
        } else {
            return res.status(400).send("Invalid parameter").end();
        }
    });
    /**
     * Get list of ResgetRestaurant 
     */
    app.get('/api/entity', bodyParser.json(), async (req, res) => {
        return await EntityController.getEntities(res);
    });

    app.get('/api/entity/current', [AuthMiddleware.auth(),bodyParser.json()], async (req, res) => {
        return await EntityController.getCurrentEntity(req, res);
    });

    app.get('/api/entity/:idEntity',[AuthMiddleware.auth(),bodyParser.json()], async (req, res) => {
        if (req.params.idEntity) {
            return await EntityController.getEntity(req, res);
        } else {
            return res.status(400).send("Invalid parameter").end();
        }
    });

    /**
     *  delete ResgetRestaurant
     * @param idResgetRestaurant: url
     */
    app.delete('/api/entity/:idEntity', bodyParser.json(), async (req, res) => {
        if (req.params.idEntity) {
            return await EntityController.deleteEntity(req,res);
        } else {
            return res.status(400).send("Invalid parameter").end();
        }
    });


};
