const bodyParser = require('body-parser');
const AuthController = require('../controllers').AuthController;
const AuthMiddleware = require('../middlewares').AuthMiddleware;

const models = require('../models');
const User = models.User;
const Role = models.Role;

module.exports = function(app) {

    app.post('/api/auth/register', bodyParser.json(), async (req, res) => {
        if(req.body.firstName && req.body.lastName && req.body.dateOfBirth &&
            req.body.login && req.body.role && req.body.password && req.body.email) {
            AuthController.register(req,res);
        } else {
            return res.status(400).end();
        }
    });

    app.post('/api/auth/login', bodyParser.json(), async (req, res) => {
        if(req.body.login && req.body.password) {
                return await AuthController.login(req,res);
        } else {
            return res.status(400).end();
        }
    });

    app.post('/api/authorized', [AuthMiddleware.auth(),bodyParser.json()], async (req, res) => {
        return await AuthController.isAllowed(req,res);
    });
};
