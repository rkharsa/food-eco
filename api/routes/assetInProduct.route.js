const bodyParser = require('body-parser');
const AssetInProductController = require('../controllers').AssetInProductController;

const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {
    app.post('/api/assetInProduct',[AuthMiddleware.auth(),PermitMiddleware.permit('admin'),bodyParser.json()], async (req, res) => {
        if (req.body.idAsset && req.body.idProduct && req.body.quantity>0) {
            return await AssetInProductController.addAssetInProduct(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });


    app.put('/api/assetInProduct/asset/:idAsset/product/:idProduct', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
        if (req.params.idAsset && req.params.idProduct && (req.body.quantity>0 || req.body.newIdAsset)) {
            return await AssetInProductController.updateAssetInProduct(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });


    app.get('/api/assetInProduct/asset/:idAsset', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
        if (req.params.idAsset) {
            return await AssetInProductController.AssetComposeProducts(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });
    
       app.get('/api/assetInProduct/count/asset/:idAsset', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
        if (req.params.idAsset) {
            return await AssetInProductController.countAssetInProduct(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.get('/api/assetInProduct/product/:idProduct',  [AuthMiddleware.auth(), PermitMiddleware.permit('admin')],bodyParser.json(), async (req, res) => {
        if (req.params.idProduct) {
            return await AssetInProductController.getAssetInOneProduct(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.get('/api/assetInProduct', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])], bodyParser.json(), async (req, res) => {
        return await AssetInProductController.getAllAssetInProduct(res);
    });

    app.get('/api/assetInProduct/asset/:idAsset/product/:idProduct',[AuthMiddleware.auth(), PermitMiddleware.permit(['admin'])],  bodyParser.json(), async (req, res) => {
        if (req.params.idProduct && req.params.idAsset) {
            return await AssetInProductController.getOneAssetInOneProduct(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.put('/api/assetInProduct',  [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
        if (req.body.idAsset || req.body.idProduct) {
            return await AssetInProductController.deleteOneAssetInOneProduct(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });


};
