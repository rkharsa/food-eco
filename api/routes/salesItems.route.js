const bodyParser = require('body-parser');
const SalesItemsController = require('../controllers').SalesItemsController;
const SalesController = require('../controllers').SalesController;

const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {

   /**
   * Permet d'associer une promotion à un menu
   */
  app.post('/api/itemsSale/:idSale', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
    if (req.params.idSale && req.body.id) {
        return await SalesItemsController.AssociateItemToSale(req,res);
    } else {
        return res.status(400).json({ message: "Invalid parameter" });
    }
  });


  /**
   * Récupérer tous les menus associés à une promotion
   */
  app.get('/api/itemsSale/:idSale', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
    if (req.params.idSale) {
        return await SalesItemsController.getAllMenus(req,res);
    } else {
        return res.status(400).json({ message: "Invalid parameter" });
    }
  });

    /**
   * Permet de supprimer une association
   */
  app.delete('/api/itemsSale/:idSale', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
    if (req.params.idSale && req.body.id) {
        return await SalesItemsController.deleteAssociation(req,res);
    } else {
      return res.status(400).json({ message: "Invalid parameter" });
    }
  });


};
