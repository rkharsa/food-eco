const bodyParser = require('body-parser');
const SupplierController = require('../controllers').SupplierController;

const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {
    app.post('/api/supplier', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
        if (req.body.address && req.body.name && req.body.email && req.body.tel ) {
            return await SupplierController.addSupplier(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.put('/api/supplier/:idSupplier', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
        if (req.params.idSupplier && (req.body.email|| req.body.address || req.body.name || req.body.tel  )) {
            return await SupplierController.updateSupplier(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });
    app.get('/api/supplier', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
        return await SupplierController.getAllSupplier(req,res);
    });
    app.get('/api/supplier/:idSupplier', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
        return await SupplierController.getOneSupplier(req, res);
    });


    app.delete('/api/supplier/:idSupplier', [AuthMiddleware.auth(), PermitMiddleware.permit('admin')], bodyParser.json(), async (req, res) => {
        if (req.params.idSupplier) {
            return await SupplierController.deleteSupplier(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });


};
