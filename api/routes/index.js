const catalogRoutes = require('./catalog.route');
const auth = require('./auth.route');
const test = require('./test.route');
const promotion = require('./promotion.route');
const salesItems = require('./salesItems.route');
const orderLine = require('./orderLine.route');
const order = require('./order.route');
const unit = require('./unit.route');
const supplier = require('./supplier.route');
const asset = require('./asset.route');
const user = require('./user.route');
const lot= require('./lot.route');
const table=require('./table.route');
const assetInProduct=require('./assetInProduct.route');
const productInMenu=require('./productInMenu.route');
const entity=require('./entity.route');
const supplierDetails = require('./supplierDetails.route');
const toGive = require('./toGive.route');

module.exports = function(app) {
    test(app),
    auth(app),
    promotion(app),
    salesItems(app),
    catalogRoutes(app),
    productInMenu(app),
    orderLine(app),
    order(app),
    user(app),
    unit(app),
    supplier(app),
    asset(app),
    lot(app),
    assetInProduct(app),
    user(app),
    table(app),
    entity(app),
    supplierDetails(app),
    toGive(app)
};
