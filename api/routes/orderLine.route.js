const bodyParser = require('body-parser');
const OrderLineController = require('../controllers').OrderLineController;
const AuthMiddleware = require('../middlewares').AuthMiddleware;
const PermitMiddleware = require('../middlewares').PermitMiddleware;

module.exports = function (app) {

    app.post('/api/orderLine', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator', 'server'])], bodyParser.json(), async (req, res) => {
        if (req.body.idItem && req.body.idOrder && req.body.quantity >=0){
                return await OrderLineController.addItemInOrder(req,res);
            } else {
                return res.status(400).json({ message: "Invalid parameter" });
            }
    });
    
    app.put('/api/orderLine', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator', 'server'])], bodyParser.json(), async (req, res) => {
        if ((req.body.idOrderLine || req.body.idOrder) && (req.body.quantity>=0  || req.body.comments || req.body.hasOwnProperty("status"))) {
            return await OrderLineController.updateOrderLine(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });
    app.put('/api/orderLine/update/status', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator', 'server'])], bodyParser.json(), async (req, res) => {
        if ((req.body.idOrderLine || req.body.idOrder) && req.body.idItem && req.body.quantity>0 && req.body.hasOwnProperty("status")) {
             return await OrderLineController.updateStatus(req, res);
         } else {
             return res.status(400).json({ message: "Invalid parameter" });
         }
     });
    
    app.delete('/api/orderLine/:idOrderLine', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator', 'server'])], bodyParser.json(), async (req, res) => {
        if (req.params.idOrderLine ) {
            return await OrderLineController.deleteOrderLine(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.get('/api/orderLine/current', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator', 'server'])], bodyParser.json(), async (req, res) => {
        return await OrderLineController.getCurrentOrder(req,res);
    });

    app.get('/api/orderLine/:idOrder', [AuthMiddleware.auth(),PermitMiddleware.permit(['admin', 'preparator', 'server'])], bodyParser.json(), async (req, res) => {
        if (req.params.idOrder) {
            return await OrderLineController.getOrderOrderLines(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.delete('/api/orderLine/order/:idOrder', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator', 'server'])], bodyParser.json(), async (req, res) => {
        if (req.params.idOrder) {
            return await OrderLineController.deleteOrderOrderLines(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });

    app.post('/api/orderLine/items/sales', [AuthMiddleware.auth(), PermitMiddleware.permit(['admin', 'preparator', 'server'])],bodyParser.json(), async (req, res) => {
        if (req.body.startDate && req.body.endDate) {
            return await OrderLineController.getItemSales(req, res);
        } else {
            return res.status(400).json({ message: "Invalid parameter" });
        }
    });


};