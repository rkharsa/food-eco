module.exports = function (sequelize, DataTypes) {
    const Unit = sequelize.define('Unit', {
        idUnit: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        label: {
            type: DataTypes.STRING,
            allowNull: false
        },abrv: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        timestamps:false,
        paranoid: false,
        freezeTableName: true,
        underscored: false
    });
    return Unit;
};
