module.exports = function (sequelize) {

    const SalesItems = sequelize.define('SalesItems', {}, {
        freezeTableName: true,
        underscored: false
    });
    SalesItems.associate = (models) => {
        SalesItems.belongsTo(models.Catalog, {
            foreignKey: {
                name: "idItem",
                allowNull: false,
            },as:"catalog"
            , onDelete: 'cascade' 
        });
        SalesItems.belongsTo(models.Sales, {
            foreignKey: {
                name: "idSale",
                allowNull: false,
            },as:"sale"
            , onDelete: 'cascade' 
        });
    };
    return SalesItems;
};
