module.exports = function (sequelize, DataTypes) {
    
    const Sales = sequelize.define('Sales', {
        idSale: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        }, title: {
            type: DataTypes.STRING,
            allowNull: false
        }, description: {
            type: DataTypes.TEXT,
            allowNull: false
        }, rate: {
            type: DataTypes.FLOAT,
            allowNull: false
        }, startDate: {
            type: DataTypes.DATEONLY,
            allowNull: false
        }, endDate: {
            type: DataTypes.DATEONLY,
            allowNull: false
        }
    }, {
        freezeTableName: true,
        underscored: false,
        timestamps: true
    });
    Sales.associate = (models) => {
        Sales.belongsToMany(models.Catalog, {
            foreignKey: {
                name: "idSale",
                allowNull: false
            }, through: models.SalesItems,as:"sale"
            , onDelete: 'cascade' 
        });
    }
    //creation de la table d'association   SalesItems
  

   
    return Sales;
};
