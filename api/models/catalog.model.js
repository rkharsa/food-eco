module.exports = function (sequelize, DataTypes) {
    const Catalog = sequelize.define('Catalog', {
        idItem: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false
        },
        price: {
            type: DataTypes.FLOAT,
            allowNull: false
        }, isMenu: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },online:{
            type: DataTypes.BOOLEAN,
            allowNull: false
        },promote:{
            type: DataTypes.BOOLEAN,
            allowNull: false
        }
    }, {
        paranoid: false,
        freezeTableName: true,
        underscored: false
    });
    Catalog.associate = (models) => {
            Catalog.belongsToMany(Catalog, {
                foreignKey: {
                    name: 'idMenu',
                    allowNull: false
                },through:models.ProductInMenu
                ,as:"menu"
                , onDelete: 'cascade' 
            });
            Catalog.belongsToMany(models.Asset, {
                foreignKey: {
                    name: 'idProduct',
                    allowNull: false
                },through:models.AssetInProduct
                ,as:"productAsset"
                , onDelete: 'cascade' 
            });

            Catalog.belongsToMany(Catalog, {
                foreignKey: {
                    name: 'idProduct',
                    allowNull: false
                },through:models.ProductInMenu,as:"product"
                , onDelete: 'cascade' 
            });
            Catalog.belongsToMany(models.Sales, {
                foreignKey: {
                    name: "idItem",
                    allowNull: false
                }, through: models.SalesItems,as:"catalog"
                , onDelete: 'cascade' 
            });
            Catalog.belongsTo(models.Entity, {
                foreignKey: {
                    name: "idEntity",
                    allowNull: false
                },  onDelete: 'cascade' 
            });
 
        };
    return Catalog;
};
