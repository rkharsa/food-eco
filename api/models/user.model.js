module.exports = function (sequelize, DataTypes) {
    const User = sequelize.define('User', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        firstName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        lastName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        dateOfBirth: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false
        },
        login: {
            type: DataTypes.STRING,
            allowNull: false
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        },
        passwordSalt: {
            type: DataTypes.STRING,
            allowNull: false
        },
        contractStart: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        contractEnd: {
            type: DataTypes.DATEONLY,
            allowNull: false
        }
    }, {
        paranoid: true,
        freezeTableName: true,
        underscored: false,
        timestamps: true
    });
    User.associate = (models) => {
        User.belongsTo(models.Role, {
            foreignKey: {
                name: 'idRole'
            }
        });
        User.belongsTo(models.Entity, {
            foreignKey: {
                name: 'idEntity'
            }
        });        
    };
    return User;
};
