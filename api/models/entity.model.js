module.exports = function (sequelize, DataTypes) {
    const Entity = sequelize.define('Entity', {
        idEntity: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        isAssociation: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        address:{
            type: DataTypes.STRING,
            allowNull: false
        },
        city:{
            type: DataTypes.STRING,
            allowNull: false
        },
        latitude:{
            type: DataTypes.FLOAT,
            allowNull: false
        },
        longitude:{
            type: DataTypes.FLOAT,
            allowNull: false
        },
        tel:{
            type: DataTypes.BIGINT,
            allowNull: false
        }
    }, {
        freezeTableName: true,
        underscored: false,
        paranoid:false
    });
    return Entity;
};
