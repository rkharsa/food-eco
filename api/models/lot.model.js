module.exports = function (sequelize, DataTypes) {
    const Lot = sequelize.define('Lot', {
        idLot: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        expireDate: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        deliveryDate: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        price: {
            type: DataTypes.FLOAT,
            allowNull: false
        },
        quantity: {
            type: DataTypes.FLOAT,
            allowNull: false
        },
        currentQuantity: {
            type: DataTypes.FLOAT,
            allowNull: false
        },bio: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        }
    }, {
        paranoid: false,
        freezeTableName: true,
        underscored: false,
        timestamps: true
    });
    Lot.associate = (models) => {
        Lot.belongsTo(models.Asset, {
            foreignKey: {
                name: 'idAsset',
                allowNull :false
            }, onDelete: 'cascade' 
        });
   
        Lot.belongsTo(models.Supplier, {
            foreignKey: {
                name: 'idSupplier',
                allowNull :true
            }, onDelete: 'cascade' 
        });     

    };
    return Lot;
};
