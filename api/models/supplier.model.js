module.exports = function (sequelize, DataTypes) {
    const Supplier = sequelize.define('Supplier', {
        idSupplier: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        address: {
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false
        },
        tel: {
            type: DataTypes.BIGINT,
            allowNull: false
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        latitude:{
            type: DataTypes.FLOAT,
            allowNull: false
        },
        longitude:{
            type: DataTypes.FLOAT,
            allowNull: false
        },
    }, {
        paranoid: false,
        freezeTableName: true,
        underscored: false,
        timestamps: false
    });
    Supplier.associate = (models) => {
        Supplier.belongsTo(models.Entity, {
            foreignKey: {
                name: 'idEntity'
            }, onDelete: 'cascade' 
        })};

        return Supplier;
    };
