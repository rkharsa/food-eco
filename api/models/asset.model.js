module.exports = function (sequelize, DataTypes) {
    const Asset = sequelize.define('Asset', {
        idAsset: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        label: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        timestamps:false,
        paranoid: false,
        freezeTableName: true,
        underscored: false,
        timestamps: true
    });
    Asset.associate = (models) => {
        Asset.belongsTo(models.Unit, {
            foreignKey: {
                name: 'idUnit'
            }
        });
        Asset.belongsToMany(models.Catalog, {
            foreignKey: {
                name: 'idAsset',
                allowNull: false
            },through:models.AssetInProduct
            ,as:"assetProduct",
            onDelete: 'cascade' 
        });

        Asset.belongsTo(models.Entity, {
            foreignKey: {
                name: 'idEntity',
                allowNull: false
            },
            onDelete: 'cascade' 
        });
    };
    return Asset;
};
