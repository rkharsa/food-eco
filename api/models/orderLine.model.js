

module.exports = function (sequelize, DataTypes) {

    const OrderLine = sequelize.define('OrderLine', {
        idOrderLine: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        }
        ,quantity: {
            type: DataTypes.BIGINT,
            allowNull: false
        },
        price: {
            type: DataTypes.FLOAT,
            allowNull: false
        },
        comments: {
            type: DataTypes.STRING,
            allowNull: true
        }, status: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
    }, {
        freezeTableName: true,
        underscored: false,
        timestamps: true
    });

    OrderLine.associate = (models) => {
        OrderLine.belongsTo(models.Order, {
            foreignKey: {
                name: "idOrder",
                allowNull: false
            }, through: OrderLine,as:"order", onDelete: 'cascade' 
        });
        OrderLine.belongsTo(models.Catalog, {
            foreignKey: {
                name: "idItem",
                allowNull: false
            }, through: OrderLine, as : "catalogOrder", onDelete: 'cascade' 
        });
    }


    return OrderLine;
};
