module.exports = function (sequelize, DataTypes) {
    const assetInProduct= sequelize.define('AssetInProduct', {
        quantity: {
            type: DataTypes.FLOAT,
            allowNull: false,
        }
    }, {
        freezeTableName: true,
        underscored: false,
        timestamps: false
    });

    assetInProduct.associate = (models) => {
        assetInProduct.belongsTo(models.Asset, {
            foreignKey: {
                name: "idAsset",
                allowNull: false,
            },as:"assetProduct"
            , onDelete: 'cascade' 
        });
        assetInProduct.belongsTo(models.Catalog, {
            foreignKey: {
                name: "idProduct",
                allowNull: false,
            },as:"productAsset"
            , onDelete: 'cascade' 
        });
    };
    

    return assetInProduct;
};
