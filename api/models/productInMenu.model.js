module.exports = function (sequelize, DataTypes) {
    const ProductInMenu = sequelize.define('ProductInMenu', {
        quantity: {
            type: DataTypes.BIGINT,
            allowNull: false,
        }
    }, {
        freezeTableName: true,
        underscored: false,
        timestamps: true
    });

    ProductInMenu.associate = (models) => {
        ProductInMenu.belongsTo(models.Catalog, {
            foreignKey: {
                name: "idMenu",
                allowNull: false,
            },as:"menu"
            , onDelete: 'cascade' 
        });
        ProductInMenu.belongsTo(models.Catalog, {
            foreignKey: {
                name: "idProduct",
                allowNull: false,
            },as:"product"
            , onDelete: 'cascade' 
        });
    };
    

    return ProductInMenu;
};
