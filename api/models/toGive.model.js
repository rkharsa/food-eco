module.exports = function (sequelize, DataTypes) {
    const ToGive = sequelize.define('ToGive', {
        idToGive: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false
        },
        status: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        quantity: {
            type: DataTypes.BIGINT,
            allowNull: true
        }
    }, {
        paranoid: false,
        freezeTableName: true,
        underscored: false,
        timestamps: true
    });
    ToGive.associate = (models) => {
        ToGive.belongsTo(models.Entity, {
            foreignKey: {
                name: 'idAssociation'
            },onDelete: 'cascade' 
        });       
        ToGive.belongsTo(models.Entity, {
            foreignKey: {
                name: 'idEntity'
            },
            onDelete: 'cascade' 
        });
        ToGive.belongsTo(models.Lot, {
            foreignKey: {
                name: 'idLot'
            },
            onDelete: 'cascade' 
        });
    }
    return ToGive;
};
