module.exports = {
    AuthMiddleware: require('./auth.middleware'),
    PermitMiddleware: require('./permission.middleware')
};