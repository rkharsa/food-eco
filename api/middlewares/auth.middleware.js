const AuthController = require('../controllers').AuthController;
const jwt = require('jsonwebtoken');
require('dotenv').config();
const AuthService = require('../services').AuthService;

class AuthMiddleware {
  static auth() {
    return async function (req, res, next) {
      try {
        const authorization = req.headers['authorization'];
        if (!authorization) {
          return res.status(401).end();
        }
        const token = authorization.slice(7);

        jwt.verify(token, `${process.env.JWT_SECRET_KEY}`, (err, autorize) => {
          if (err) {
            console.log(err);
            return res.status(403).json({ message: 'Not logged in' });
          }
        });
        req.token = token;

        const user = await AuthService.userFromToken(token);
        req.user = user;

        next();
      } catch (e) {
        return res.status(400).json({ message: e.message });
      }
    };
  }
}

module.exports = AuthMiddleware;
