const models = require('../models');
const SupplierDetails= models.SupplierDetails;
class SupplierDetailsService {
 
  static async addSupplierDetails(idSupplier,idAsset) {
    try {
      return await SupplierDetails.create({idSupplier,idAsset});
    } catch (e) {
      console.log(e);
      throw Error(e);
    }

  }

  static async deleteSupplierDetails(idSupplierDetails) {
    try {
      return await SupplierDetails.destroy({ where: { idSupplierDetails } });
    } catch (e) {
      throw Error('Error while fetching SupplierDetailss data');
    }
  }
  static async deleteSupplierDetailsBySupplier(idSupplier) {
    try {
      return await SupplierDetails.destroy({ where: { idSupplier } });
    } catch (e) {
      throw Error('Error while deleting SupplierDetails data');
    }
  }



  /**
   * Permet de recuperer un SupplierDetailss
   * @param {Number} idSupplierDetails
   * @returns {Promise<SupplierDetailss>}
   */
  static async getSupplierDetails(idSupplier) {
    try {
      return await SupplierDetails.findAll({
        where: { idSupplier },include:[{model:models.Asset,include:[models.Unit]}]
      });
    } catch (e) {
      console.log(e);
      throw Error('Error while fetching SupplierDetails data');
    }

  }
  static async getAllSupplierDetails(idEntity) {
    try {
      return await SupplierDetails.findAll({
      include:[
        {
         model: models.Supplier,
         where:{idEntity}
        },{
          model:models.Asset,
          include:[models.Unit]
        }
      ]
      });
    } catch (e) {
      console.log(e);
      throw Error('Error while fetching SupplierDetails data');
    }

  }


 

}

module.exports = SupplierDetailsService;
