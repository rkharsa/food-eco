const models = require('../models');
const ToGive = models.ToGive;
const LotService = require('./lot.service');
const OutputService = require('./outputLot.service');

class ToGiveService {
     static async addToGive(description, quantity, idLot, idEntity) {
        try {
            const toGive =  await ToGive.create({
                description,
                status: 0,
                quantity,
                idLot,
                idEntity
            }).then(async a => {
                await LotService.calcNewQuantity(idLot, idEntity, quantity);
            });
            return toGive;
        } catch (e) {
            console.log(e);
            throw Error('Error while creating ToGive');
        }
    }

    static async getAllToGive(idEntity) {
        try {
            return await ToGive.findAll({where:{idEntity}});
        } catch (e) {
            throw Error('Error while fetching ToGive data');
        }
    }

    static async getAssociationToGive(idEntity) {
        try {
            return await ToGive.findAll({where:
                {idAssociation: idEntity},
                include:[models.Entity,
                    {model:
                    models.Lot,
                    include:[{model:
                        models.Asset,
                        include:[models.Unit]
                    }]
                }]
            });
        } catch (e) {
            throw Error('Error while fetching ToGive data');
        }

    }

    static async getAvailableToGive() {
        try {
            return await ToGive.findAll({where:
                {status: 0},
                include:[models.Entity,
                    {model:
                    models.Lot,
                    include:[{model:
                        models.Asset,
                        include:[models.Unit]
                    }]
                }]
            });
        } catch (e) {
            throw Error('Error while fetching ToGive data');
        }

    }

    static async bookToGive(idToGive, idEntity) {
        try {
            return await ToGive.update({status: 1, idAssociation: idEntity}, { where: { idToGive } })
        } catch (e) {
            throw Error('Error while booking ToGive');
        }
    }

    static async changeStatus(idToGive, status, idLot, quantity) {
        try {
            let obj = {status: status};
            if(status == 0) {
                obj.idAssociation = null;
            }
            if(status == 2) {
                await OutputService.addOutputLot(idLot, quantity);
            }
            return await ToGive.update(obj , { where: { idToGive } })
        } catch (e) {
            throw Error('Error while updating ToGive status');
        }
    }

    static async updateToGive(idToGive, description, quantity, status, idEntity) {
        const toGive = await this.getToGive(idToGive);
        const oldQuantity = toGive.quantity;
        let object = {};
        let keyTable = ["description", "quantity", "status"];
        for (let i = 1; i < arguments.length; i++) {
        if (arguments[i] !== "")
            object[`${keyTable[i - 1]}`] = arguments[i];
        }
        try {
        const result = await ToGive.update(object, { where: { idToGive } }).then(async a => {
            await LotService.updateQuantity(toGive.idLot, idEntity, oldQuantity, quantity);
        });
        return result;
    } catch (e) {
        console.log(e.message);
      throw Error('Error while  updating toGive');
    }

  }

    static async getToGive(idToGive) {
        try {
            return await ToGive.findOne({
                where: {
                    idToGive
                }
            });
        } catch (e) {
            throw Error('Error while fetching ToGive');
        }

    }

    static async deleteToGive(idToGive, idEntity) {
        try {
            const deletedToGive = await this.getToGive(idToGive);
            const result = await ToGive.destroy({
                where: {
                    idToGive
                }
            }).then(async a => {
                await LotService.calcNewQuantityAfterToGiveDelete(deletedToGive.idLot, idEntity, deletedToGive.quantity);
            });
            return result;
        } catch (e) {
            console.log(e.message);
            throw Error('Error while deleting ToGive');

        }

    }

    static async getPercentageStats(idLot, idEntity){
        try {
            const toGiveQuantity = await ToGive.sum('quantity', { where: { idLot: idLot, status: 2 }});
            const lot = await LotService.getLot(idLot, idEntity);
            const usedQuantity = lot.quantity - lot.currentQuantity + toGiveQuantity;

            var today = new Date();
            const expiredQuantity = new Date(lot.expireDate) <= today ? lot.currentQuantity : 0;
            const result = {
                idLot: lot.idLot,
                givenQuantity: (toGiveQuantity*100/lot.quantity).toFixed(2),
                usedQuantity: (usedQuantity*100/lot.quantity).toFixed(2),
                expiredQuantity: (expiredQuantity*100/lot.quantity).toFixed(2),
                totalQuantity: lot.quantity
            };
            return result;
        } catch (e) {
            throw Error('Error while fetching ToGive data');
        }
    }


}
module.exports = ToGiveService;