const models = require('../models');
const Table = models.Table;
const Order = models.Order;
class TableService {
     static async addTable(description, name, numberOfSeats, idEntity) {
        try {
            return await Table.create({
                description,
                name,
                numberOfSeats,
                idEntity
            });
        } catch (e) {
            throw Error('Error while creating table');
        }

    }
    // TODO faire une fonction qui permet de faire la somme des item commandé   et ensuite l'implement a l"ajout d'un produit comme ca a chaque ajout le prix total est mis a joiur
    static async getAllTables(idEntity) {
        try {
            return await Table.findAll({where:{idEntity}});
        } catch (e) {
            console.log(e);
            throw Error('Error while fetching table data');
        }

    }

    static async updateTable(id, description, name, numberOfSeats) {
    let object = {};
    let keyTable = ["description", "name", "numberOfSeats"];
    for (let i = 1; i < arguments.length; i++) {
      if (arguments[i] !== "")
        object[`${keyTable[i - 1]}`] = arguments[i];
    }
    try {
      return await Table.update(object, { where: { idTable: id } });
    } catch (e) {
      throw Error('Error while  updating table');
    }

  }

    static async getTable(idTable) {
        try {
            return await Table.findOne({
                where: {
                    idTable
                }
            });
        } catch (e) {
            throw Error('Error while fetching table');
        }

    }

    static async deleteTable(idTable) {
        try {
            return await Table.destroy({
                where: {
                    idTable
                }
            });
        } catch (e) {
            throw Error('Error while fetching table');

        }

    }

}
module.exports = TableService;