const models = require('../models');
const Catalog = models.Catalog;
const CatalogService = require('./catalog.service');
const ProductInMenu = models.ProductInMenu;

class ProductInMenuService {

  /**
   * Permet d'ajouter un produit dans un menu 
   * @param {Number} idProduct 
   * @param {Number} idMenu 
   * @param {Number} quantity 
   * @returns {Promise<ProductInMenu>}
   */
  static async addProductInMenu(idProduct, idMenu, quantity) {
    try {
      if (!await CatalogService.menuExist(idMenu) || !await CatalogService.productExist(idProduct)) {
        throw Error(' idMenu or IdProduct not exist in Catalog | 400 ');
      }
      const [productInMenu, created] = await ProductInMenu.findOrCreate({
        where: { idProduct: idProduct, idMenu: idMenu },
        defaults: {
          idProduct: idProduct,
          idMenu: idMenu,
          quantity: quantity
        }
      });
      if (created == 0)
        throw Error("Association already exist | 400 ");
      return productInMenu;
    } catch (e) {
      throw Error(e);
    }

  }
  /**
   * Permet de mettre a jour un produit dans un menu  ou un menu dans un produit
   * @param {Number} idProduct 
   * @param {Number} idMenu 
   * @param {Number} quantity 
   * @param {Number} newIdProduct 
   * @returns {Promise<ProductInMenu>} 
   */

  static async updateProductInMenu(idProduct, idMenu, quantity, newIdProduct) {
    let object = {};
    try {
      if (newIdProduct) {
        if (!await CatalogService.productExist(newIdProduct))
          throw Error('Error newIdProduct not exist in Catalog | 400');
        if (await this.getOneProductInMenu(idMenu, newIdProduct) != 0)
          throw Error('Error this association already exist | 400');
        object.idProduct = newIdProduct;
      }


      if (quantity>=0)
        object.quantity = quantity;


      return await ProductInMenu.update(object, { where: { idMenu, idProduct } });
    } catch (e) {
      throw Error(e);
    }

  }
  //TODO est ce que il doit etre possible de changer un id Menu dans productinmenu ou il faut juste
  /**
   * Permet de recuperer l'ensemble des menus avec les produits
   * @returns {Promise<ProductInMenu>}
   */
  static async getAllMenusDetails() {
    try {
      return await ProductInMenu.findAll({
        order: [
          ['idMenu', 'DESC'],
        ], include: [{
          model: Catalog,
          as: 'product'
        }, {
          model: Catalog,
          as: 'menu'
        }]
      });
    } catch (e) {
      throw Error('Error while fetching product in menu data');
    }
  }

  /**
   * Permet de recuperer les details d'un menu
   * @param {Number} idMenu 
   * @returns {Promise<ProductInMenu>}
   */
  static async getMenuDetails(idMenu) {
    try {
      return await ProductInMenu.findAll({
        where: { idMenu },
        order: [
          ['idMenu', 'DESC'],
        ], include: [{
          model: Catalog,
          as: 'product'
        }, {
          model: Catalog,
          as: 'menu'
        }]
      });
    } catch (e) {
      throw Error('Error while fetching product in menu data');
    }
  }

  /**
   * Permet de recuperer une ligne dans la table product in menu
   * @param {Number} idMenu 
   * @param {Number} idProduct 
   * @returns {Promise<ProductInMenu>}
   */
  static async getOneProductInMenu(idMenu, idProduct) {
    try {
      return await ProductInMenu.findAll({
        where: { idMenu, idProduct }
        , include: [{
          model: Catalog,
          as: 'product'
        }, {
          model: Catalog,
          as: 'menu'
        }]
      });
    } catch (e) {
      throw Error('Error while fetching product in menu data');
    }
  }

  /**
   * Permet de supprimer un produit dans un menu 
   * @param {Number} idMenu 
   * @param {Number} idProduct 
   * @returns {Promise<ProductInMenu>}
   */
  static async deleteProductInMenu(idMenu, idProduct) {
    try {
     let obj= {};
      if(idMenu)
      obj.idMenu=idMenu;
      if(idProduct)
      obj.idProduct
      return await ProductInMenu.destroy({
        where:  obj
      });

    } catch (e) {
      throw Error('Error while deleting product in menu data');
    }
  }

  /**
   * Permet de recuperer les menus affecté a un produit
   * @param {Number} idProduct 
   * @returns {Promise<ProductInMenu>}
   */
  static async productComposeMenus(idProduct) {
    try {
      return await ProductInMenu.findAll({
        where: { idProduct },
        order: [
          ['idMenu', 'DESC'],
        ], include: [{
          model: Catalog,
          as: 'product'
        }, {
          model: Catalog,
          as: 'menu'
        }]
      });
    } catch (e) {
      throw Error('Error while fetching product in menu data');
    }
  }


}

module.exports = ProductInMenuService;
