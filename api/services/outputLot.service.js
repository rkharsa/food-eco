const models = require('../models');
const OutputLot= models.OutputLot;
class OutputLotService {
 
  static async addOutputLot(idLot,quantity) {
    try {
      return await OutputLot.create({idLot,quantity});
    } catch (e) {
      throw Error(e);
    }

  }

  static async getStatsPerDay(startDate, endDate, idLot) {
        const sqlRequest = "SELECT MIN(quantity) as quantity, idLot, DATE(createdAt) AS date FROM outputlot "+
        "WHERE DATE(createdAt) BETWEEN ? AND ? AND idLot=? GROUP BY DATE(createdAt),idLot";
        return await models.sequelize.query(sqlRequest,
            {
                replacements: [startDate,endDate,idLot],
                type: models.sequelize.QueryTypes.SELECT
        });
  }



}

module.exports = OutputLotService ;
