const models = require('../models');
const Catalog = models.Catalog;
const SalesItems = models.SalesItems;
const Sequelize = require('sequelize').QueryTypes;
const Sales = models.Sales;
class CatalogService {
  /**
   * Permet d'ajouter un produit ou un menu 
   * @param {String} title 
   * @param {Number} price
   * @param {Number} isMenu
   * @param {Number} online
   * @returns {Promise<Catalog>}
   */
  static async addCatalog(title, price, isMenu, online, promote,idEntity) {
    try {
      return await Catalog.create({ title, price, isMenu, online, promote,idEntity });
    } catch (e) {
      throw Error('Error while creating Catalog');
    }

  }

  /**
   * Permet de mettre a jour un produit ou un menu
   * @param {Number} id 
   * @param {String} title 
   * @param {Number} price 
   * @param {boolean} isMenu 
   * @param {boolean} online 
   * @returns {Promise<Catalog>}
   */
  static async updateCatalog(id, title, price, isMenu, online, promote) {
    let object = {};
    let keyTable = ["title", "price", "isMenu", "online", "promote"];
    for (let i = 1; i < arguments.length; i++) {
      if (arguments[i] !== "")
        object[`${keyTable[i - 1]}`] = arguments[i];
    }
    try {
      return await Catalog.update(object, { where: { idItem: id } });
    } catch (e) {
      throw Error('Error while  updating Catalog');
    }

  }

  /**
   * Permet de supprimer un produit ou un menu
   * @param {Number} idItem 
   * @returns {Promise<Catalog>}
   */
  static async deleteCatalog(idItem,idEntity) {
    try {
      return await Catalog.destroy({ where: { idItem,idEntity } });
    } catch (e) {
      console.log(e);
      throw Error('Error while fetching catalog data');
    }

  }

  /**
   * Permet de recuperer l'ensemble des produits et menus
   * @returns {Promise<Catalog>}
   */
  static async getCatalogs(idEntity) {
    try {
      return await Catalog.findAll({where:{idEntity}});
    } catch (e) {
      console.log(e);
      throw Error('Error while fetching catalog data');
    }

  }

  /**
   * Permet de verfier si le produit existe dans la table catalog
   * @param {Number} idItem 
   */
  static async productExist(idItem) {
    try {
      return await Catalog.findOne({ where: { idItem, isMenu: 0 } });
    } catch (e) {
      console.log(e);
      throw Error('Error while fetching catalog data');
    }

  }

  /**
   * Permet de verfier si un menu exist e dans la table catalog
   * @param {Number} idItem 
   * @returns {Promise<Catalog>}
   */
  static async menuExist(idItem) {
    try {
      return await Catalog.findOne({ where: { idItem, isMenu: 1} });
    } catch (e) {
      console.log(e);
      throw Error('Error while fetching catalog data');
    }

  }

  /**
   * Permet de recuperer un produit ou un menu 
   * @param {Number} idItem 
   * @returns {Promise<Catalog>}
   */
  static async getCatalog(idItem,idEntity) {
    try {
      return await Catalog.findOne({
        where: { idItem,idEntity }
      });
    } catch (e) {
      throw Error('Error while fetching catalog data');
    }

  }
  static async getCatalogsPromote(promote) {
    try {
      return await Catalog.findAll({
        where: { promote }
      });
    } catch (e) {

      throw Error('Error while fetching catalog data');
    }

  }
  //TODO ajouter id Entity a cette requete
  static async  getCatalogWitSale() {
    try {
      return await models.sequelize.query("SELECT DISTINCT(C.idItem),C.title,IF(idSale IS NOT NULL, ROUND(price-price*(SELECT SUM(rate) FROM salesitems B INNER JOIN sales A on B.idSale=A.idSale WHERE idItem=I.idItem AND NOW() BETWEEN startDate AND endDate),2) ,C.price) as price,isMenu,online FROM SalesItems I RIGHT JOIN catalog C ON C.idItem=I.idItem ", {
        model: Catalog,
        mapToModel: true // pass true here if you have any mapped fields
      })
    } catch (e) {
      throw Error('Error while fetching catalog data');
    }
  }
  //TODO les menus etproduit  mis en ligne
  static async  getCatalogWitSaleOnline() {
    try {
      return await models.sequelize.query("SELECT DISTINCT(C.idItem),C.title,IF(idSale IS NOT NULL, ROUND(price-price*(SELECT SUM(rate) FROM salesitems B INNER JOIN sales A on B.idSale=A.idSale WHERE idItem=I.idItem AND NOW() BETWEEN startDate AND endDate ),2) ,C.price) as price,isMenu,online FROM SalesItems I RIGHT JOIN catalog C ON C.idItem=I.idItem WHERE online=1", {
        model: Catalog,
        mapToModel: true // pass true here if you have any mapped fields
      })
    } catch (e) {
      throw Error('Error while fetching catalog data');
    }
  }

  /**
   * Permet de  personnaliser la requete si online ou si c'est un menu
   * @param {Boolean} online 
   * @param {Boolean} isMenu 
   */
  static async getCatalogCustom(online, isMenu,idEntity) {
    let object = {};
    if (typeof online === "boolean")
      object.online = online;

    if (typeof isMenu === "boolean")
      object.isMenu = isMenu;

      object.idEntity=idEntity;
    try {
      return await Catalog.findAll({ where: object });
    } catch (e) {
      throw Error('Error while fetching catalog data');
    }

  }

  /**
   * Permet d'obtenir le prix depuis un id OrderLine
   * @param {Number} idItem 
   */
  static async getCatalogPrice(idItem) {
    try {
      return await Catalog.findOne({
        where: { idItem },
        order: [
          ['price', 'DESC'],
        ]
      });
    } catch (e) {
      throw Error('Error while fetching catalog data');
    }
  }
}

module.exports = CatalogService;
