const models = require('../models');
const Lot = models.Lot;
const { Op } = require("sequelize");
const SupplierService = require("./supplier.service");
const AssetService = require("./asset.service");
const CatalogService = require("./catalog.service");
const ProductInMenu = require("./productInMenu.service");
const AssetInProduct = require("./assetInProduct.service");
const OutputLotService = require("./outputLot.service");

class LotService {
    static async addLot(expireDate, deliveryDate, quantity, price, idAsset, idSupplier, bio) {
        try {
            var date = new Date();
            const now = date.getFullYear() + "-" + ('0' + (date.getMonth() + 1)).slice(-2) + "-" + ('0' + (date.getDate())).slice(-2);
            if (now > expireDate)
                throw Error("expire date will be superior or equal of current date | 400 ");

            if (!await AssetService.getAsset(idAsset))
                throw Error("id Asset not exist | 400");


            return await Lot.create({
                expireDate,
                deliveryDate,
                quantity,
                currentQuantity: quantity,
                price,
                idAsset,
                idSupplier,
                bio
            });
        } catch (e) {
            throw Error(e);
        }

    }
    static async isAvailableRouter(idItem, idEntity, quantityOrder) {
        const isMenu = await CatalogService.menuExist(idItem) ? 1 : 0;
        const isProduct = await CatalogService.productExist(idItem) ? 1 : 0;
        if (isMenu) {
            return this.menuIsAvailable(idItem, idEntity, quantityOrder);
        } else if (isProduct) {
            return this.productAvailable(idItem, idEntity, quantityOrder);
        }
    }
    static async menuIsAvailable(idMenu, idEntity, quantityOrder) {
        try {
            const productInMenu = await ProductInMenu.getMenuDetails(idMenu);
            let tab = [];
            if (productInMenu == 0)
                return false;

            for (let prodInMenu of productInMenu) {
                const isAvailable = await this.productAvailable(prodInMenu.idProduct, idEntity, quantityOrder);
                if (Math.floor(isAvailable) < prodInMenu.quantity)
                    return 0;

                if (prodInMenu.quantity != 0) {
                    tab.push(isAvailable / prodInMenu.quantity);
                } else {
                    tab.push(isAvailable);
                }

            }
            return Math.min(...tab);
        } catch (e) {
            throw Error("while aggregating data ");
        }
    }
    static async productAvailable(idItem, idEntity, quantityOrder) {
        try {
            const assetInOneProduct = await AssetInProduct.getAssetInOneProduct(idItem);
            let tabQuantityPerAsset = [];
            if (assetInOneProduct == 0)
                return false;

            for (let assetInProd of assetInOneProduct) {
                const sumQuantity = await this.getSumAssetsQuantityAvailable(assetInProd.idAsset, idEntity);
                if (assetInProd.quantity != 0) {
                    tabQuantityPerAsset.push((sumQuantity.sum / assetInProd.quantity) / quantityOrder);
                } else {
                    tabQuantityPerAsset.push(sumQuantity.sum / quantityOrder);
                }

            }
            return Math.min(...tabQuantityPerAsset);

        } catch (e) {
            throw Error("while fetching if product is available");
        }
    }
    static async getSumAssetsQuantityAvailable(idAsset, idEntity) {
        const sqlRequest = "SELECT SUM(currentQuantity) as sum" +
            " FROM Lot L INNER JOIN supplier S ON S.idSupplier=L.idSupplier " +
            " WHERE  currentQuantity>=0 AND S.idEntity =?" +
            " AND NOW()<=expireDate  AND idAsset=?  ";
        return await models.sequelize.query(sqlRequest,
            {
                replacements: [idEntity, idAsset],
                plain: true
            });
    }
    static async getProductWillExpireFirst(idAsset, idEntity) {
        try {
            const sqlRequest = "SELECT expireDate,currentQuantity,idLot,quantity" +
                " FROM Lot L  INNER JOIN supplier S ON S.idSupplier=L.idSupplier " +
                " WHERE  currentQuantity>=0  AND S.idEntity= ? " +
                " AND NOW()<=expireDate  AND idAsset=? Order by expireDate ASC ";
            return await models.sequelize.query(sqlRequest,
                {
                    replacements: [idEntity, idAsset],
                    type: models.sequelize.QueryTypes.SELECT
                });
        } catch (e) {
            throw Error("while getting product will expire first");
        }
    }

    static allocateQuantityOrder(productWillExpire, quantityOrder, menuQuantity, assetInProdQuantity, i, rest) {
        let calcQuantity, result, quantitySub;
        if (i >= 1) {
            calcQuantity = productWillExpire.currentQuantity - rest;
            quantitySub = rest;
        } else {
            calcQuantity = productWillExpire.currentQuantity - (quantityOrder * (menuQuantity * assetInProdQuantity));
            quantitySub = quantityOrder * (menuQuantity * assetInProdQuantity)
        }

        if (calcQuantity < 0) {
            rest = Math.abs(calcQuantity);
            result = calcQuantity + rest;
            quantitySub -= rest;
        } else {
            rest = 0;
            result = calcQuantity;
        }
        return { rest: rest, result: result, quantitySub: quantitySub };
    }
    static async calcCurrentQuantity(assetInProd, prodInMenu, isMenu, quantityOrder, idEntity) {
        try {
            const menuQuantity = isMenu ? prodInMenu.quantity : 1;
            let productWillExpireFirst = await this.getProductWillExpireFirst(assetInProd.idAsset, idEntity);
            let rest, i, resp;
            for (let productWillExpire of productWillExpireFirst) {
                resp = this.allocateQuantityOrder(productWillExpire, quantityOrder, menuQuantity, assetInProd.quantity, i, rest);
                rest = resp.rest;

                await this.updateLot(productWillExpire.idLot, "", "", "", "", resp.result, "", "");

                await OutputLotService.addOutputLot(productWillExpire.idLot, resp.result);
                if (rest == 0) {
                    break;
                }
                i++;
            }
        } catch (e) {
            console.log(e);
            throw Error("while the treatment");
        }
    }
    static async  assetsInOneProductIteration(idProduct, prodInMenu, isMenu, quantityOrder, idEntity) {
        try {
            const assetInOneProduct = await AssetInProduct.getAssetInOneProduct(idProduct);

            assetInOneProduct.map(async function (assetInProd) {
                await LotService.calcCurrentQuantity(assetInProd, prodInMenu, isMenu, quantityOrder, idEntity);
            });
        } catch (e) {
            throw Error("while iterate assets in one product");
        }
    }
    static async calcCurrentQuantityProduct(idProduct, quantityOrder, operator, idEntity) {
        await this.assetsInOneProductIteration(idProduct, "", 0, quantityOrder, operator, idEntity);
    }
    static async calcCurrentQuantityMenu(idMenu, quantityOrder, idEntity) {
        try {
            const productInMenu = await ProductInMenu.getMenuDetails(idMenu);

            productInMenu.map(async function (prodInMenu) {
                await LotService.assetsInOneProductIteration(prodInMenu.idProduct, prodInMenu, 1, quantityOrder, idEntity);
            });
        } catch (e) {
            throw Error("while aggregating data ");
        }
    }
    static async  calcCurrentQuantityRouter(idItem, quantityOrder, idEntity) {
        try {
            const isMenu = await CatalogService.menuExist(idItem) ? 1 : 0;
            const isProduct = await CatalogService.productExist(idItem) ? 1 : 0;

            if (isMenu) {
                this.calcCurrentQuantityMenu(idItem, quantityOrder, idEntity)
            } else if (isProduct) {
                this.calcCurrentQuantityProduct(idItem, quantityOrder, idEntity);
            }
        } catch (e) {
            throw Error("while aggregating data ");
        }
    }
    static async getLots(idEntity) {
        try {
            return await Lot.findAll({ include: [{ model: models.Supplier, where: { idEntity } }, { model: models.Asset, include: [models.Unit] }] });
        } catch (e) {
            throw Error('Error while fetching Lot data');
        }
    }

    static async getLotsWithAssetAndUnit(idEntity) {
        try {
            return await Lot.findAll({
                include: [
                    { model: models.Supplier, where: { idEntity } },
                    {
                        model:
                            models.Asset,
                        include: [models.Unit]
                    },
                ]
            });
        } catch (e) {
            throw Error('Error while fetching Lot data');
        }
    }

    static async getLotsAvailableOrNot(idEntity, notExpire = 1) {
        const operator = notExpire ? Op.gt : Op.lt;
        const arithmeticOp = notExpire ? Op.and : Op.or;
        try {
            return await Lot.findAll({
                where:
                {
                    [arithmeticOp]: {
                        currentQuantity: {
                            [operator]: 0
                        }
                        , expireDate: {
                            [operator]: Date.now()
                        }
                    }

                },
                include: [
                    { model: models.Supplier, where: { idEntity } },
                    {
                        model:
                            models.Asset,
                        include: [models.Unit]
                    }]
            });
        } catch (e) {
            console.log(e);
            throw Error('Error while fetching Lot data');
        }
    }


    static async getLotsBySupplier(idSupplier, idEntity) {
        try {
            return await Lot.findAll({ where: { idSupplier }, include: [models.Asset, { model: models.Supplier, where: { idEntity } }] });
        } catch (e) {
            throw Error('Error while fetching Lot data');
        }
    }

    //TODO verifier que la quantité courante est inferieur a la quantity total et superieur a 0  et verifier que le produit n'est pas deja perimé au changement de expire
    static async updateLot(idLot, expireDate, deliveryDate, price, quantity, currentQuantity, bio, idAsset, idSupplier, idEntity) {
        let object = {};
        const date = new Date();
        const now = date.getFullYear() + "-" + ('0' + (date.getMonth() + 1)).slice(-2) + "-" + ('0' + (date.getDate())).slice(-2);
        let keyTable = ["expireDate", "deliveryDate", "price", "quantity", "currentQuantity", "bio", "idAsset", "idSupplier"];
        for (let i = 1; i < arguments.length; i++) {
            if (arguments[i] !== "")
                object[`${keyTable[i - 1]}`] = arguments[i];
        }
        if (expireDate && expireDate < now) {
            throw Error("expire date will be superior or equal of current date | 400 ");
        }
        if (idAsset && !await AssetService.getAsset(idAsset)) {
            throw Error("id Asset not exist | 400");
        }

        if (idSupplier && !await SupplierService.getOneSupplier(idSupplier, idEntity)) {
            throw Error("id Supplier not exist  | 400");
        }
        try {
            return await Lot.update(object, { where: { idLot } });
        } catch (e) {
            throw Error(e);
        }

    }

    static async calcNewQuantity(idLot, idEntity, removedQuantity) {
        try {
            const lot = await this.getLot(idLot, idEntity);
            const newQuantity = lot.currentQuantity - removedQuantity;
            return await Lot.update({ currentQuantity: newQuantity }, { where: { idLot } });
        } catch (e) {
            throw Error('Error while calculating new Lot quantity');
        }
    }

    static async updateQuantity(idLot, idEntity, oldToGiveQuantity, newToGiveQuantity) {
        try {
            const lot = await this.getLot(idLot, idEntity);
            let quantity = lot.currentQuantity + oldToGiveQuantity - newToGiveQuantity;
            return await Lot.update({ currentQuantity: quantity }, { where: { idLot } });
        } catch (e) {
            throw Error('Error while calculating new Lot quantity');
        }
    }

    static async calcNewQuantityAfterToGiveDelete(idLot, idEntity, quantity) {
        try {
            const lot = await this.getLot(idLot, idEntity);
            let newQuantity = lot.currentQuantity + quantity;
            return await Lot.update({ currentQuantity: newQuantity }, { where: { idLot } });
        } catch (e) {
            throw Error('Error while calculating new Lot quantity');
        }
    }

    static async getLot(idLot, idEntity) {
        try {
            return await Lot.findOne({ where: { idLot }, include: [models.Asset, { model: models.Supplier, where: { idEntity } }] });
        } catch (e) {
            throw Error('Error while fetching Lot');

        }

    }

    static async deleteLot(idLot) {
        try {
            return await Lot.destroy({ where: { idLot } });
        } catch (e) {
            throw Error('Error while fetching Lot');
        }
    }

}
module.exports = LotService;