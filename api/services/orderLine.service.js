const models = require('../models');
const Order = models.Order;
const Catalog = models.Catalog;
const OrderLine = models.OrderLine;
const CatalogService = require("./catalog.service");
const OrderService = require("./order.service");
const LotService = require("./lot.service");
class OrderLineService {
    static async addItemInOrder(idItem, idOrder, quantity, comments = null, idEntity) {
        try {
            if (!await CatalogService.getCatalog(idItem, idEntity) || !await OrderService.getOneOrder(idOrder))
                throw Error("idItem or IdOrder not exist | 400  ");

            if (Math.floor(await LotService.isAvailableRouter(idItem, idEntity, quantity)) == 0)
                throw Error("Product is not available | 400  ");


            let calcPrice = await this.getPriceWitSale(idItem);

            const orderLine = await OrderLine.create({
                quantity,
                price: calcPrice.price * quantity,
                comments,
                idOrder,
                idItem,
                status: 0
            })

            await this.updateOrderPrice(idOrder);
            return orderLine;
        } catch (e) {
            throw Error(e);
        }

    }
    //TODO recuperer l'ensemble des orderline qui  ont un status non cuisiner ( c pour les cuisiniers ca )
    // status : en attente , en cours de cuisine , fini de cuisiner,attente de livraison, servie
    static async getCurrentOrder(idEntity) {
        try {
            //inferieur a 3 voir now il renvoie quoi normalement que la date du j 
            return await OrderLine.findAll({
                include: [{ model: Catalog, as: "catalogOrder" }, { model: Order, where: { isPaid: false, idEntity }, as: "order" }],
                order: [
                    ['createdAt', 'DESC']
                ]
            })
        } catch (e) {
            throw Error('Error while fetching catalog data');
        }
    }
    static async  getPriceWitSale(idItem) {
        try {
            return await models.sequelize.query("SELECT IF(idSale IS NOT NULL, ROUND(price-price*(SELECT sum(B.rate) FROM SalesItems A INNER JOIN Sales B ON B.idSale=A.idSale WHERE A.idItem=? AND NOW() BETWEEN B.startDate AND B.endDate group by idItem ),2) ,C.price) as price,isMenu,online FROM SalesItems I RIGHT JOIN catalog C ON C.idItem=I.idItem WHERE C.idItem=? ", {
                replacements: [idItem, idItem],
                plain: true
            })
        } catch (e) {
            throw Error('Error while fetching catalog data');
        }
    }

    static async updateOrderPrice(id) {
        try {
            let totalPrice = await this.getOrderPrice(id);
            return await Order.update(
                { priceTotal: totalPrice },
                { where: { idOrder: id } }
            );

        } catch (e) {
            throw Error('Error while updating order');

        }
    }

    static async  getOrderPrice(idOrder) {
        try {
            const result = await models.sequelize.query("SELECT SUM(price) AS price FROM OrderLine WHERE idOrder=?", {
                replacements: [idOrder],
                plain: true
            });
            return result.price;
        } catch (e) {
            throw Error('Error while modifying price');
        }
    }

    static async calcTotalPrice(idItem, quantity) {
        try {
            const orderLine = await this.getPriceWitSale(idItem);
            if (!orderLine)
                throw Error("orderLine not exist | 400");
            LotService.calcCurrentQuantityRouter(idItem, quantity);
            return orderLine.price * quantity;
        } catch (e) {
            throw Error(e);
        }
    }


    //TODO dans order quand on va supprimer une order entiere il faut pour chaque orderline faire appel a cette fonction puis supprimer order  
    static async deleteOrderLine(idOrderLine, idEntity) {
        try {
            const orderLine = await this.getOrderLine(idOrderLine);
            if (orderLine) {
                return await OrderLine.destroy({ where: { idOrderLine } });
            }
            return;
        } catch (e) {
            throw Error(e);
        }
    }

    static async deleteOrderOrderLines(idOrder, idEntity) {
        try {
            const orderLines = await this.getOrderLineByIdOrder(idOrder).then(
                a => {
                    a.map(orderLine => {
                        if (orderLine.status == 0) {
                            this.deleteOrderLine(orderLine.idOrderLine, idEntity);
                        }
                    })
                }
            );
            return;
        } catch (e) {
            throw Error(e);
        }
    }
    static async getOrderLine(idOrderLine) {
        try {
            return await OrderLine.findOne({
                where: { idOrderLine }
                , include: [{
                    model: Catalog,
                    as: "catalogOrder"
                }, {
                    model: Order,
                    as: "order"
                }]
            });
        } catch (e) {
            throw Error('Error while fetching product in menu data');
        }
    }

    //TODO dans order quand on va supprimer une order entiere il faut pour chaque orderline faire appel a cette fonction puis supprimer order  

    static async deleteOrder(idOrder, idEntity) {
        try {
            const orderLine = await this.getOrderLineByIdOrder(idOrder);
            orderLine.map(async function (obj) {
                try {
                    await OrderLineService.deleteOrderLine(obj.idOrderLine, idEntity);
                } catch (e) {
                    throw Error("error while deleting orderlines")
                }
            });
            return await Order.destroy({
                where: {
                    idOrder
                }
            });

        } catch (e) {
            throw Error('Error while deleting order');

        }

    }

    static async getOrderLineByIdOrder(idOrder) {
        try {
            return await OrderLine.findAll({
                where: { idOrder }
                , include: [{
                    model: Catalog,
                    as: "catalogOrder"
                }, {
                    model: Order,
                    as: "order"
                }]
            });
        } catch (e) {
            throw Error('Error while fetching product in menu data');
        }
    }

    static async updateOrderLine(idOrderLine, quantity, comments, status, idOrder, idEntity) {
        let object = {};
        let objectWhere = {};
        try {
            if (idOrderLine && quantity) {
                const orderLine = await this.getOrderLine(idOrderLine);
                // verifier le status actuel 
                if (quantity) {
                    object.quantity = quantity;
                    const price = await this.calcTotalPrice(orderLine.idItem, quantity);
                    object.price = price;
                    if (!price)
                        throw Error("Error: calc price total | 400 ");
                }
            }

            if (comments)
                object.comments = comments;

            if (Number.isInteger(status)) {
                object.status = status;
            }


            if (idOrder)
                objectWhere.idOrder = idOrder;

            if (idOrderLine)
                objectWhere.idOrderLine = idOrderLine;

            return await OrderLine.update(object, { where: objectWhere });

        } catch (e) {
            throw Error(e);
        }

    }
    static async updateStatus(idOrder, idOrderLine, idItem, status, quantityOrder, idEntity) {
        try {
            let objectWhere = {};
            if (status <= 2) {
                if (status == 0) {
                    if (Math.floor(await LotService.isAvailableRouter(idItem, idEntity, quantityOrder)) == 0)
                        throw Error("Product is not available | 400  ");
                    LotService.calcCurrentQuantityRouter(idItem, quantityOrder, idEntity);

                }
                if (status == 2) {
                    status = 1;
                } else {
                    status += 1;
                }
                if (idOrder)
                    objectWhere.idOrder = idOrder;

                if (idOrderLine)
                    objectWhere.idOrderLine = idOrderLine;
                return await OrderLine.update({ status }, { where: objectWhere });
            }
        } catch (e) {
            throw Error(e);
        }

    }


    // permet d'avoir le nombre de item ecouler par jour 
    static async countItemSalePerDay(startDate, endDate, item = null) {
        let sqlRequest, tabReplacements;

        if (item == null) {
            sqlRequest = "SELECT o.idItem, C.title,SUM(quantity) AS salesNumber ,DATE(o.createdAt) AS date" +
                " FROM orderline o INNER JOIN catalog C on o.idItem=C.idItem WHERE DATE(o.createdAt) BETWEEN ? AND ? GROUP BY o.idItem, DATE(o.createdAt) ORDER BY o.idItem";
            tabReplacements = [startDate, endDate];
        } else {
            sqlRequest = "SELECT O.idItem, C.title, SUM(O.quantity) AS salesNumber,DATE(O.createdAt) AS date" +
                " FROM orderline O INNER JOIN catalog C on O.idItem=C.idItem WHERE DATE(O.createdAt)  BETWEEN ? AND ? AND O.idItem=? GROUP BY O.idItem, DATE(O.createdAt)  ";
            tabReplacements = [startDate, endDate, item];
        }

        return await models.sequelize.query(sqlRequest,
            {
                replacements: tabReplacements,
                type: models.sequelize.QueryTypes.SELECT
            });
    }

}
module.exports = OrderLineService;