require('dotenv').config();
const express = require('express');
const models = require('./models');
const routes = require('./routes');
const cors = require('cors');
const mysql = require('mysql2');
const bodyParser = require('body-parser');
const Socket = require('./socket/socket');
const Sentry = require('@sentry/node');
const Tracing = require('@sentry/tracing');
const path = require('path');

async function bootstrap() {
  try {
    await models.sequelize.authenticate();
    await models.sequelize.sync(); //{force:true}
  } catch (e) {
    console.log(e);
  }
  const app = express();

  Sentry.init({
    dsn:
      'https://9a9d32ac9eed4f028012efce0486279a@o481901.ingest.sentry.io/5531282',
    integrations: [
      // enable HTTP calls tracing
      new Sentry.Integrations.Http({ tracing: true }),
      // enable Express.js middleware tracing
      new Tracing.Integrations.Express({ app }),
    ],

    // We recommend adjusting this value in production, or using tracesSampler
    // for finer control
    tracesSampleRate: 1.0,
  });

  // RequestHandler creates a separate execution context using domains, so that every
  // transaction/span/breadcrumb is attached to its own Hub instance
  app.use(Sentry.Handlers.requestHandler());
  // TracingHandler creates a trace for every incoming request
  app.use(Sentry.Handlers.tracingHandler());

  // The error handler must be before any other error middleware and after all controllers
  app.use(Sentry.Handlers.errorHandler());

  // Optional fallthrough error handler
  app.use(function onError(err, req, res, next) {
    // The error id is attached to `res.sentry` to be returned
    // and optionally displayed to the user for support.
    res.statusCode = 500;
    res.end(res.sentry + '\n');
  });

  app.use(cors());
  routes(app);
  app.get('/debug-sentry', function mainHandler(req, res) {
    throw new Error('My first Sentry error!');
  });
  app.use(
    bodyParser.urlencoded({
      extended: true,
    })
  );

  app.use(express.static('public/FoodEco'));

  var server = app.listen(process.env.PORT || 3001, () => {
    console.log(`Listening on ${process.env.PORT}`);
  });
  Socket.initialize(server);
  app.get('/', function (req, res) {
    console.log(path.join(__dirname));
    res.sendFile(path.join(__dirname + '/public/FoodEco/index.html'));
  });
}

bootstrap();
