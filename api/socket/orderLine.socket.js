class OrderLineSocket {
    
    static start(io) {
        io.on('connection', (socket) => {

            socket.on('updateOrderLine', data => {
                socket.broadcast.emit('updatedOrderLine', data);
            });
            
        });
    }
}

module.exports = OrderLineSocket;