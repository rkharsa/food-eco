const Socket=require('../socket/socket');

class OrderSocket {
    
    static start(io) {
        io.on('connection', (socket) => {

            socket.on('updateOrder', data => {
                socket.broadcast.emit('updatedOrder', data);
            });

            socket.on('updateTableOrder', data => {
                socket.broadcast.emit('updatedTableOrder', data);
            });

            socket.on('createOrder', data => {
                socket.broadcast.emit('createdOrder', data);
            });

            socket.on('deleteOrder', data => {
                socket.broadcast.emit('deletedOrder', data);
            });
        });
    }
}

module.exports = OrderSocket;
