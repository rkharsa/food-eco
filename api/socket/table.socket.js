class TableSocket {
    
    static start(io) {
        io.on('connection', (socket) => {

            socket.on('updateTable', () => {
                socket.broadcast.emit('updatedTable');
            });
            
        });
    }
}

module.exports = TableSocket;