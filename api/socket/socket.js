//const express = require('express');
//const server = require('http').Server(express);
//const io = require("socket.io")(server, { origins: '*:*'});
const cors = require('cors');
const OrderSocket = require("../socket/order.socket");
const OrderLineSocket = require("../socket/orderLine.socket");
const TableSocket = require("../socket/table.socket");

class Socket {
    static initialize(server) {
        var io = require('socket.io').listen(server);

        OrderSocket.start(io);
        OrderLineSocket.start(io);
        TableSocket.start(io);
    }
}

module.exports = Socket;
