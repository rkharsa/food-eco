const base64 = require('base64url');
const PermitService = require("../services").PermitService;

class PermitController {

    static async isAllowed(req,res,next,allowed) {
        try {
            if (!PermitService.isAllowed(req.token, allowed)) {
                return res.status(403).json({"message": "User doesn't have access"});
            } else {
                next(); 
            }
        } catch (e) {
              return res.status(500).json({ message: e.message });
        }
    }

}

module.exports = PermitController;
