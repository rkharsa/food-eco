module.exports = {
    AuthController: require('./auth.controller'),
    PermitController: require('./permission.controller'),
    CatalogController :require('./catalog.controller'),
    ProductInMenuController:require('./productInMenu.controller'),
    OrderController : require('./order.controller'),
    OrderLineController: require('./orderLine.controller'),
    SalesController: require('./promotion.controller'),
    SalesItemsController: require('./salesItems.controller'),
    UserController: require('./user.controller'),
    UnitController: require('./unit.controller'),
    SupplierController:require('./supplier.controller'),
    AssetController:require('./asset.controller'),
    LotController:require('./lot.controller'),
    TableController:require('./table.controller'),
    AssetInProductController:require('./assetInProduct.controller'),
    EntityController : require('./entity.controller'),
    EmailController : require('./email.controller'),
    SupplierDetailsController : require('./supplierDetails.controller'),
    ToGiveController : require('./toGive.controller')

}
