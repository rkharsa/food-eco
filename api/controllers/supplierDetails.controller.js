const SupplierDetailsService=require('../services').SupplierDetailsService;


class SupplierDetailsController {

  static async addSupplierDetails(req,res) {
    const data=req.body;
      try {
        const SupplierDetails = await SupplierDetailsService.addSupplierDetails(data.idSupplier,data.idAsset);
        if (!SupplierDetails ) {
          return res.status(204).end();
        }
        return res.status(201).json(SupplierDetails);
      } catch (e) {
        return res.status(500).json({ message: e.message });

      }
  }



  static async deleteSupplierDetails(req,res) {
    try {
      const SupplierDetails = await SupplierDetailsService.deleteSupplierDetails(req.params.idSupplierDetails);
      if (!SupplierDetails)
        return res.status(204).end();

      return res.status(200).json(SupplierDetails);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }

  }
  static async deleteSupplierDetailsBySupplier(req,res) {
    try {
      const SupplierDetails = await SupplierDetailsService.deleteSupplierDetailsBySupplier(req.params.idSupplier);
      if (!SupplierDetails)
        return res.status(204).end();

      return res.status(200).json(SupplierDetails);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }

  }

  static async getSupplierDetails(req,res) {
    try {
      const SupplierDetails = await SupplierDetailsService.getSupplierDetails(req.params.idSupplier);
      if (SupplierDetails == 0)
        return res.status(204).end();

      return res.status(200).json(SupplierDetails);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }

  }

  static async getAllSupplierDetails(req,res) {
    try {
      const SupplierDetails = await SupplierDetailsService.getAllSupplierDetails(req.user.idEntity);
      if (SupplierDetails == 0)
        return res.status(204).end();

      return res.status(200).json(SupplierDetails);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }

  }


 

}

module.exports = SupplierDetailsController;
