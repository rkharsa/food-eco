const models = require('../models');
const Catalog = models.Catalog;
const CatalogService=require('../services').CatalogService;


class CatalogController {

  static async addCatalog(req,res) {
      try {
        const catalog = await CatalogService.addCatalog(req.body.title, req.body.price,
          req.body.isMenu, req.body.online,req.body.promote,req.user.idEntity);
        if (!catalog ) {
          return res.status(204).end();
        }
        return res.status(201).json(catalog);
      } catch (e) {
        return res.status(500).json({ message: e.message });
      }
  }

  static async updateCatalog(req,res) {
    try {
      const catalog = await CatalogService.updateCatalog(req.params.idItem, req.body.title, req.body.price,
        req.body.isMenu, req.body.online,req.body.promote);
      if (!catalog )
        return res.status(204).end();

      return res.status(200).json(catalog);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }
  }
  
/**
 * Permet de supprimer un produit ou un menu
 * @param {Number} idItem 
 * @returns {Promise<Catalog>}
 */
  static async deleteCatalog(req,res) {
    try {
      const catalog = await CatalogService.deleteCatalog(req.params.idItem,req.user.idEntity);
      if (!catalog)
        return res.status(204).end();

      return res.status(200).json(catalog);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }

  }

  static async getCatalogs(req,res) {
    try {
      const catalog = await CatalogService.getCatalogs(req.user.idEntity);
      if (catalog==0)
        return res.status(204).end();

      return res.status(200).json(catalog);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }

  }


 
  static async getCatalog(req,res) {
    try {
      const catalog = await CatalogService.getCatalog(req.params.idItem,req.user.idEntity);
      if (!catalog )
        return res.status(204).end();

      return res.status(200).json(catalog);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }

  }
 
  static async getCatalogCustom(req,res) {
    try {
      const catalog = await CatalogService.getCatalogCustom(req.body.online, req.body.isMenu,req.user.idEntity);
      if (!catalog)
        return res.status(204).end();

      return res.status(200).json(catalog);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }
  }
 
  static async getCatalogPrice(req,res) {
    try {
      const catalog = await CatalogService.getCatalogPrice(req.body.isMenu);
      if (!catalog)
        return res.status(204).end();

      return res.status(200).json(catalog);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }
  }
}

module.exports = CatalogController;
