const models = require('../models');
const Sales = models.Sales;
const SalesService=require('../services').SalesService

class SalesController {

  /**
   * @param title
   * @param description
   * @param rate
   * @param startDate
   * @param endDate
   * @returns {Promise<Sales>}
   */
  static async  addSale(req,res) {
    try {
      const sale = await SalesService.addSale(req.body.title,
        req.body.description,
        req.body.rate,
        req.body.startDate,
        req.body.endDate);

       return res.status(201).json(sale);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }
  }

  /**
   * @param id
   * @param title
   * @param description
   * @param rate
   * @param startDate
   * @param endDate
   * @returns {Promise<Sales>}
   */
  static async updatePromotion(req,res) {
    try {
      const sale = await SalesService.updatePromotion(req.params.idSale,
        req.body.title, req.body.description,
        req.body.rate, req.body.startDate,
        req.body.endDate);

      if (!sale)
        return res.status(204).end();

      return res.status(200).json(sale);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }
  }
/**
 * Supprimer une promotion
 * @param {Number} idSale 
 * @returns {Promise<Sales>}
 */
  static async deleteSale(req,res) {
    try {
      const sale = await SalesService.deleteSale(req.params.idSale);
      if (!sale)
         return res.status(204).end();

      return res.status(200).json(sale);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }
  }

  /**
   *  Recuperer l'ensemble des promotions
   * @returns {Promise<Sales>}
   */
  static async getSales(res) {
    try {
      const sale = await SalesService.getSales();
      if (!sale)
          return res.status(204).end();

      return res.status(201).json(sale);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }
  }
  /**
   * Recuperer une promotion
   * @param {Number} idSale 
   * @returns {Promise<Sales>}
   */
  static async getOneSale(req,res) {
    try {
      const sale = await SalesService.getOneSale(req.params.idSale);
      if (!sale)
          return res.status(204).end();

      return res.status(201).json(sale);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }
  }

}

module.exports = SalesController;
