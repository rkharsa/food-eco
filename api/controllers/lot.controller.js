
const LotService = require("../services").LotService;
const OutputLotService = require("../services").OutputService;
class LotController {
    
    static async addLot(req, res) {
        const data = req.body;
        try {
            const Lot = await LotService.addLot(data.expireDate, data.deliveryDate, data.quantity, data.price, data.idAsset, data.idSupplier,data.bio);
            if (!Lot) {
                return res.status(204).end();
            }
            return res.status(201).json(Lot);
        } catch (e) {
            const messageSplit = e.message.split("|");
            const status = messageSplit[1] ? messageSplit[1] : 500;
            const message = messageSplit[0];
            return res.status(status).json({ message: message });
        }

    }

    static async getLots(req,res) {
        try {
            const Lot = await LotService.getLots(req.user.idEntity);
            if (Lot == 0)
                return res.status(204).end();
            return res.status(200).json(Lot);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getLotsAssetUnit(req,res) {
        try {
            const Lot = await LotService.getLotsWithAssetAndUnit(req.user.idEntity);
            if (Lot == 0)
                return res.status(204).end();
            return res.status(200).json(Lot);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getAvailableLots(req,res) {
        try {
            const Lot = await LotService.getLotsAvailableOrNot(req.user.idEntity);
            if (Lot == 0)
                return res.status(204).end();
            return res.status(200).json(Lot);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }
    static async getExpireLots(req,res) {
        try {
            const Lot = await LotService.getLotsAvailableOrNot(req.user.idEntity,0);
            if (Lot == 0)
                return res.status(204).end();
            return res.status(200).json(Lot);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getLotsBySupplier(req,res) {
        try {
            const Lot = await LotService.getLotsBySupplier(req.params.idSupplier,req.user.idEntity);
            if (Lot == 0)
                return res.status(204).end();
            return res.status(200).json(Lot);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async getStatsPerDay(req,res) {
        try {
            const Lot = await OutputLotService.getStatsPerDay(req.body.startDate,req.body.endDate, req.params.idLot);
            if (Lot == 0)
                return res.status(204).end();
            return res.status(200).json(Lot);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async updateLot(req, res) {
        const dataParams = req.params;
        const dataBody = req.body;
        try {
            const Lot = await LotService.updateLot(dataParams.idLot,
                dataBody.expireDate,
                dataBody.deliveryDate,
                dataBody.price,
                dataBody.quantity,
                dataBody.currentQuantity,
                dataBody.bio,
                dataBody.idAsset
                , dataBody.idSupplier,
                req.user.idEntity);
            if (!Lot)
                return res.status(204).end();

            return res.status(200).json(Lot);

        } catch (e) {
            const messageSplit = e.message.split("|");
            const status = messageSplit[1] ? messageSplit[1] : 500;
            const message = messageSplit[0];
            return res.status(status).json({ message: message });
        }


    }
    static async calcCurrentQuantityRouter(req, res) {
        const dataParams = req.params;
        const dataBody = req.body;
        try {
            const Lot = await LotService.calcCurrentQuantityRouter(dataBody.idItem, dataBody.quantityOrder, dataBody.operator, req.user.idEntity)
            if (!Lot)
                return res.status(204).end();

            return res.status(200).json(Lot);

        } catch (e) {

            return res.status(200).json({ message: e.message });
        }


    }
    static async isAvailableRouter(req, res) {
        try {
            const Lot = await LotService.isAvailableRouter(req.params.idItem);

            return res.status(200).json(Lot);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }
    static async getLot(req, res) {
        try {
            const Lot = await LotService.getLot(req.params.idLot,req.user.idEntity);
            if (!Lot)
                return res.status(204).end();

            return res.status(200).json(Lot);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }
    // TODO  pouvoir recuperer les commandes en cours les 3 etats d'une commande sont a definir quand elle est paye
    // en att en cours et donner et payer   

    static async deleteLot(req, res) {
        try {
            const Lot = await LotService.deleteLot(req.params.idLot);
            if (!Lot)
                return res.status(204).end();

            return res.status(200).json(Lot);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

}
module.exports = LotController;