const AssetService=require('../services').AssetService;


class AssetController {

  static async addAsset(req,res) {
    const data=req.body;
      try {
        const Asset = await AssetService.addAsset(data.label,data.idUnit,req.user.idEntity);
        if (!Asset ) {
          return res.status(204).end();
        }
        return res.status(201).json(Asset);
      } catch (e) {
        const messageSplit = e.message.split("|");
        const status = messageSplit[1]?messageSplit[1]:500;
        const message= messageSplit[0];

        return res.status(status).json({ message: message});

      }
  }

  static async updateAsset(req,res) {
    const dataParams=req.params;
    const dataBody= req.body;
    try {
      const Asset = await AssetService.updateAsset(dataParams.idAsset,dataBody.label,dataBody.idUnit);
      if (!Asset )
        return res.status(204).end();

      return res.status(200).json(Asset);
    } catch (e) {
      const messageSplit = e.message.split("|");
      const status = messageSplit[1]?messageSplit[1]:500;
      const message= messageSplit[0];
      return res.status(status).json({ message: message});
    }
  }

  static async deleteAsset(req,res) {
    try {
      const Asset = await AssetService.deleteAsset(req.params.idAsset,req.user.idEntity);
      if (!Asset)
        return res.status(204).end();

      return res.status(200).json(Asset);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }

  }

  static async getAssets(req,res) {
    try {
      const Assets = await AssetService.getAssets(req.user.idEntity);
      if (Assets == 0)
        return res.status(204).end();

      return res.status(200).json(Assets);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }

  }

 
  static async getAsset(req,res) {
    try {
      const Asset = await AssetService.getAsset(req.params.idAsset);
      if (!Asset )
        return res.status(204).end();

      return res.status(200).json(Asset);
    } catch (e) {
      return res.status(500).json({ message: e.message });
    }

  }

}

module.exports = AssetController;
