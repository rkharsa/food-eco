
const UnitService = require("../services").UnitService;
class UnitController {
    static async addUnit(req, res) {
        try {
            const unit = await UnitService.addUnit(req.body.label,req.body.abrv);
            if (!unit) {
                return res.status(204).end();
            }
            return res.status(201).json(unit);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

    static async getAllUnit(res) {
        try {
            const unit = await UnitService.getAllUnit();
            if (!unit)
                return res.status(204).end();
            return res.status(201).json(unit);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }
    }

    static async updateUnit(req, res) {
        try {
            const unit = await UnitService.updateUnit(req.params.idUnit,req.body.label,req.body.abrv);
            if (!unit)
                return res.status(204).end();

            return res.status(200).json(unit);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }


    }

    static async getOneUnit(req, res) {
        try {
            const unit = await UnitService.getOneUnit(req.params.idUnit);
            if (!unit)
                return res.status(204).end();

            return res.status(201).json(unit);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }
// TODO  pouvoir recuperer les commandes en cours les 3 etats d'une commande sont a definir quand elle est paye
// en att en cours et donner et payer   

    static async deleteUnit(req, res) {
        try {
            const Unit = await UnitService.deleteUnit(req.params.idUnit);
            if (!Unit)
                return res.status(204).end();

            return res.status(200).json(Unit);

        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

}
module.exports = UnitController;