const models = require('../models');
const Entity = models.Entity;
const EntityService=require('../services').EntityService;



class EntityController {


    static async addEntity(req, res) {
        try {
            const entity = await EntityService.addEntity(req.body.isAssociation, req.body.address, req.body.name,
              req.body.city, req.body.tel);
            if (entity == 0) {
              return res.status(204).end();
            }
            return res.status(201).json(entity);
          } catch (e) {
            return res.status(500).json({ message: e.message });
          }

    }

    static async updateEntity(req, res) {
        try {
            const entity = await EntityService.updateEntity(req.params.idEntity, req.body.isAssociation, req.body.name, req.body.address,
              req.body.city, req.body.tel);
            if (!entity )
              return res.status(204).end();
      
            return res.status(200).json(entity);
        } catch (e) {
            return res.status(500).json({ message: e.message });
        }

    }

    static async deleteEntity(req,res) {
        try {
            const entity = await EntityService.deleteEntity(req.params.idEntity);
            if (!entity)
              return res.status(204).end();
      
            return res.status(200).json(entity);
          } catch (e) {
            return res.status(500).json({ message: e.message });
          }
    }

    static async getEntities(res) {
        try {
            const entities = await EntityService.getEntities();
            if (!entities)
              return res.status(204).end();
      
            return res.status(200).json(entities);
          } catch (e) {
            return res.status(500).json({ message: e.message });
          }
    }
    
    static async getCurrentEntity(req, res) {
      try {
          const entity = await EntityService.getEntity(req.user.idEntity);
          if (!entity )
            return res.status(204).end();
    
          return res.status(200).json(entity);
        } catch (e) {
          return res.status(500).json({ message: e.message });
        }
  }

    static async getEntity(req, res) {
        try {
            const entity = await EntityService.getEntity(req.params.idEntity);
            if (!entity )
              return res.status(204).end();
      
            return res.status(200).json(entity);
          } catch (e) {
            return res.status(500).json({ message: e.message });
          }
    }
}

module.exports = EntityController;
